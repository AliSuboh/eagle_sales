var cart_list;
$(".product_id").click(function(){

    var  product_id = $(this).val();

    var csrf = $('[name="_csrf"]').attr('value');

    $.ajax({
        type: "POST",
        url: "/addcart?_csrf=" + csrf,
        data: {_csrf: csrf,product_id:product_id},
        success: function (data) {
            var data = JSON.parse(data)
            if(data.status){

                cart_list = data.cart_list;
                $('.product_id_'+product_id).addClass("disabled");

                $('.product_id_'+product_id).text(function(  ) {
                        return 'Added'
                    }
                )

                $(this).innerText = 'HOII';
                window.location.reload();
            }

        },
        error: function (xhr) {
            console.log('error')
        }
    });
});

$(".removefromcart").click(function(){

    var  product_id = $(this).val();
    var csrf = $('[name="_csrf"]').attr('value');

    $.ajax({
        type: "POST",
        url: "/removecart?_csrf=" + csrf,
        data: {_csrf: csrf,product_id:product_id},
        success: function (data) {
            var data = JSON.parse(data);
            $(this).parent().parent().remove();
            updateAmount();
        },
        error: function (xhr) {
            console.log('error')
        }
    });
});
$(".shopping-cart").on("click", ".removefromcart", function(event) {
    $(this).closest("tr").remove();
});

$(".refresh").click(function(){window.location.reload()});

$('[id="quantities"]').on('change',
    function () {
        var key = $(this).data("key");
        var quantity = $(this).val();
        var price = $('#p_price'+key).val();
        // var quantity = this.value;
        $('#price'+key).html('$' +(price * quantity));
        updateAmount();
        // console.log(total_amount,total_discount);
    }
);
function updateAmount() {
    var new_itemNumber = 0;
    var total_discount = parseFloat($('#total_discount').text().substr( 1 + $('#total_discount').text().indexOf('$'))) || 0;
    var total_amount = parseFloat($('#total_price').text().substr( 1 + $('#total_discount').text().indexOf('$'))) || 0;
    var new_total_amount = 0;
    var new_total_discount = 0;
    $('.shopping-cart tr').each(function() {
        if(typeof this != "undefined"){
            new_itemNumber +=1;
            var price = parseFloat($(this).find(".price").find("input").val());
            var quantity = parseInt($(this).find(".quantity ").find("input").val());
            var discount = parseFloat($(this).find(".discount").val());
            // console.log(discount);
            var discount_amount = ((discount*price)/100);
            // console.log(discount_amount*quantity,quantity)
            var value_discounted = (price*quantity) - (discount_amount*quantity);
            new_total_discount +=discount_amount*quantity;
            new_total_amount += value_discounted;
        }
    });
    if(new_total_amount > 0)
        total_amount = new_total_amount;
    if(new_total_discount > 0)
        total_discount = new_total_discount;

    $('#Items_cart_COUNT').html("<b>"+new_itemNumber+" items</b>");
    $('#total_discount').html('- $' +total_discount);
    $('#total_price').html('$' +total_amount);
    
}

