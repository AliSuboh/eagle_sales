<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/site.css',
        'css/main.css',
        'css/payment.css',
        'css/font-awesome.min.css',
        'css/shopping-cart.css',
        'css/login.css',
//        'css/main-green.css',
//        'css/main-grey.css',
//        'css/main-orange.css',
//        'css/main-red.css',

        'css/bootstrap.css',
        'css/bootstrap.min.css',
        'css/bootstrap-theme.css',
        'css/icomoon-social.css',
        'css/coming-soon-social.css',
        'css/leaflet.css',
        'css/leaflet.ie.css',

    ];
    public $js = [

        'js/main-menu.js',
        'js/modernizr-2.6.2-respond-1.1.0.min.js',
        'js/cart.js',
        'js/user.js',
        'js/jquery-1.9.1.min.js',
        'js/jquery.bxslider.js',
        'js/jquery.fitvids.js',
        'js/jquery.sequence.js',
        'js/jquery.sequence-min.js',
        'js/template.js',
        'js/bootstrap.js',
        'js/refresh.js',
        'js/details.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
