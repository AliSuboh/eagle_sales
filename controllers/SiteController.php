<?php

namespace app\controllers;

use app\models\Module;
use app\models\Payment;
use app\models\ProductDetails;
use app\models\ProductsCategoris;
use app\models\ProductSubDetails;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
//use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Products;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
//        $categories = ProductsCategoris::find()->asArray()->all();

//        return $this->render('features',['categories'=>$categories]);
        return $this->render('index');
    }



    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $categories = ProductsCategoris::find()->asArray()->all();

        return $this->render('about',['categories'=>$categories]);
    }
    public function actionShoppingCart()
    {
        $session = Yii::$app->session;
        $cart_bag = $discounted_array= [];
        $total_amount = $total_discount = 0;

        $cart_list =($session['cart_list'])?$session['cart_list']:[];
        if(!empty($cart_list)){

            $cart_bag = Products::find()->where(['PRODUCT_ID' => $cart_list])->asArray()->all();
            if(empty($cart_bag))
                Yii::$app->session->setFlash('error', 'Please add to cart first');
            else{
                $discount_calc = Module::discountCalc($cart_bag);
                $total_amount = $discount_calc['totalAmount'];
                $total_discount = $discount_calc['totalDiscount'];
                $discounted_array = $discount_calc['discount'];

            }

        }


        return $this->render('shopping-cart',['cart_bag' => $cart_bag, 'total_discount' => $total_discount,'total_amount' => $total_amount,'discounted_array' => $discounted_array]);
    }

    public function actionProducts()
    {
        try{
            $categories = ProductsCategoris::find()->asArray()->all();


        }catch (\Exception $exception){
            var_dump($exception->getMessage(),$exception->getTraceAsString());die;
        }


        return $this->render('products',['categories'=>$categories]);
    }
    public function actionDetails()
    {

        $session = Yii::$app->session;

        $product_id = $_GET['product_id'];
        $cart_list =($session['cart_list'])?$session['cart_list']:[];

        $product = Products::find()->where(['PRODUCT_ID' => $product_id])->asArray()->one();
        $categories = ProductsCategoris::find()->select(['CATEGORY_ID','CATEGORY_NAME_EN'])->asArray()->all();
//        var_dump($product_id);die;
        $product_details = ProductDetails::find()->where(['PRODUCT_ID' => $product_id])->asArray()->all();
//        echo '<pre>';
//        var_dump($product,$categories,$product_details);die;

        if(!empty($product) && !empty($categories) && !empty($product_details)){
//            echo '<pre>';
//            print_r($product_details);die;
            return $this->render('details',['product' => $product, 'categories' => $categories, 'cart_list' => $cart_list ,'product_details' =>$product_details]);

        }
        Yii::$app->session->setFlash('error', 'Sorry, Details of the product  not found,');
        return $this->redirect('/ProductsList/Software');
    }

    public function actionSubDetails($id)
    {
//var_dump($id);die;

        $product_sub_details = ProductSubDetails::find()->where(['PRODUCT_DTL_ID' => $id])->asArray()->all();

        if(!empty($product_sub_details)){
//            echo '<pre>';
//            print_r($product_sub_details);die;
//            return json_encode(['status' => true,'result' =>$product_sub_details]);
            return $this->renderPartial('_sub-details',['data'=>$product_sub_details]);
        }
            return false;
//            return json_encode(['status' => false,'result' => null]);
    }

    public function actionProductsList()
    {

        $session = Yii::$app->session;
        $cart_list =($session['cart_list'])?$session['cart_list']:[];

        $category = ProductsCategoris::find()->where(['CATEGORY_ID' => $_GET['category_id']])->asArray()->one();
//        print_r($category);die;
        if(!empty($category) && isset($category['CATEGORY_ID']) && !empty($category['CATEGORY_ID'])){
            $allProducts = Products::find()->where( ['CATEGORY_ID' => $category['CATEGORY_ID']] )->asArray()->all();

            if(!empty($allProducts)){
                $all_Products_count = sizeof($allProducts);

//                print_r($allProductsNew);die;

                return $this->render('ProductsList',['products' => $allProducts, 'cart_list' => $cart_list ,'path'=>'Products' ,'products_count'=>$all_Products_count ]);
            }
            return $this->render('ProductsList',['error',"We don't have any product on ".$category['CATEGORY_NAME_EN'] ]);

        }

        return $this->render('products');


    }

    public function actionAddcart()
    {
        $session = Yii::$app->session;
        $cart_list =($session['cart_list'])?$session['cart_list']:[];

        $product_id = $_POST['product_id'];

        $product = Products::find()->where(['PRODUCT_ID' => $product_id])->asArray()->one();

        if(isset($product) && !empty($product)){

            if(!in_array($product_id,$cart_list))
                $cart_list [] = $product_id;

            $session['cart_list'] = $cart_list;

            return json_encode(['status' => true, 'cart_list' => $cart_list]);
        }

        return json_encode(['status' => false, 'cart_list' => $cart_list]);

    }
    public function actionRemovecart()
    {
        $session = Yii::$app->session;
        $cart_list =($session['cart_list'])?$session['cart_list']:[];
        $product_id = $_POST['product_id'];
        $product = Products::find()->where(['PRODUCT_ID' => $product_id])->asArray()->one();
        if(isset($product) && !empty($product)){
                if (($key = array_search($product_id, $cart_list)) !== false)
                    unset($cart_list[$key]);

              $session['cart_list'] = $cart_list ;

            return json_encode(['status' => true, 'cart_list' => $cart_list]);
        }

        return json_encode(['status' => false, 'cart_list' => $cart_list]);

    }
    public function actionCheckoutBags(){
        $session = Yii::$app->session;
        $cart_list =($session['cart_list'])?$session['cart_list']:[];

        if( empty($cart_list) || !isset($cart_list))
            return false;

        try {
            if (empty($session['user_id'])) {
                return $this->redirect('/login-user');


            }




//            print_r($cart_bag);die;

            $total_amount = 0;
            $sub_total=0;
            $discount = 0;
            $cart_bag = Products::find()->where(['PRODUCT_ID' => $cart_list])->asArray()->all();
                foreach ($cart_bag as $key => $value){
                    $sub_total +=$value['PRODUCT_PRICE'];
                    if($value['DISCOUNT'] > 0){
                        $dis_value = ($value['PRODUCT_PRICE']*$value['DISCOUNT'])/100;
                        $discount +=$dis_value;

                        $total_amount += $value['PRODUCT_PRICE'] - $dis_value;

                    }else{
                        $total_amount += $value['PRODUCT_PRICE'];
                    }



            }

            if(!empty($_POST)){

                $payment = new Payment();
                $result = $payment->validatePayment($_POST);
                if($result['status']){
                    $session->set($session['user_id'].'payment_info',$_POST);
//                    var_dump($session->get($session['user_id'].'payment_info'));die;
                }else{
                    foreach ($result['message'] as $key => $error){
                        Yii::$app->session->setFlash('error', $error);

                    }
                }
//                print_r($result);die;

            }
        }catch (\Exception $e){
            return $this->redirect(Yii::$app->homeUrl);

        }
            return $this->render('checkout_bags',['cart_bag' => $cart_bag, 'discount' => $discount,'total_amount' => $total_amount ,'sub_total'=>$sub_total]);


    }
    public function actionPasswordReset()
    {
        return $this->render('password-reset');
    }
    public function actionCredits()
    {
        return $this->render('credits');
    }
    public function actionContactUs()
    {
        return $this->render('contact');
    }
    public function actionPayment()
    {
        $session = Yii::$app->session;

        if (empty($session['user_id'])) {
            Yii::$app->session->setFlash('error', 'Please Login first');
            return $this->redirect('/login-user');
        }
        $cart_list =($session['cart_list'])?$session['cart_list']:[];

        if( empty($cart_list) || !isset($cart_list)){
            Yii::$app->session->setFlash('error', 'Sorry, Your cart is empty!');
            return $this->redirect('/Features');

        }

            $total_amount = 0;
            $sub_total = 0;
            $discount = 0;
            $cart_bag = Products::find()->where(['PRODUCT_ID' => $cart_list])->asArray()->all();
            foreach ($cart_bag as $key => $value) {
                $sub_total += $value['PRODUCT_PRICE'];
                if ($value['DISCOUNT'] > 0) {
                    $dis_value = ($value['PRODUCT_PRICE'] * $value['DISCOUNT']) / 100;
                    $discount += $dis_value;

                    $total_amount += $value['PRODUCT_PRICE'] - $dis_value;

                } else {
                    $total_amount += $value['PRODUCT_PRICE'];
                }


            }
        $model = new Payment();

//        if(!empty($_POST)){

            $model = new Payment();
//        $model->load(Yii::$app->request->post());
//            var_dump($model->validate());die;
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $result = $model->callGetway() ) {
                var_dump('go to buy page here');die;
                if($result['status']){
                    $session->set($session['user_id'].'payment_info',$_POST);
                }else{
                    foreach ($result['message'] as $key => $error){
                        Yii::$app->session->setFlash('error', $error);

                    }
                }
            }

//        }



        return $this->render('payment', [
            'model' => $model,
            'cart_bag' => $cart_bag,
            'discount' => $discount,
            'total_amount' => $total_amount ,
            'sub_total'=>$sub_total
        ]);
    }

}
