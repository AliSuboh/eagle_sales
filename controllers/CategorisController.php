<?php

namespace app\controllers;

use Yii;
use app\models\ProductsCategoris;
use app\models\AddProductsCategoris;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategorisController implements the CRUD actions for ProductsCategoris model.
 */
class CategorisController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductsCategoris models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AddProductsCategoris();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductsCategoris model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductsCategoris model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductsCategoris();

        try {
            if (isset($_POST) && !empty($_POST) && isset($_POST['ProductsCategoris']) && !empty($_POST['ProductsCategoris'])) {

                $Categoris = $_POST['ProductsCategoris'];
                $model->attributes = $Categoris;
                $model->CREATED_DATE = date("Y-m-d H:i:s");
                $model->UPDATED_DATE = "0000-00-00 00:00:00";

                if ($model->validate()) {
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->CATEGORY_ID]);
                    }
                }
                //Show error message.
                $errors = [];
                foreach ($model->getErrors() as $key => $value) {
                    foreach ($value as $k => $msg) {
                        $errors[] = $msg;
                    }
                }

            }
        }catch (\Exception $exception){
            return $this->render('create', [
                'model' => $model,
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProductsCategoris model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        try {
            if (isset($_POST) && !empty($_POST) && isset($_POST['ProductsCategoris']) && !empty($_POST['ProductsCategoris'])) {

                $Categoris = $_POST['ProductsCategoris'];
                $model->attributes = $Categoris;
                $model->UPDATED_DATE = date("Y-m-d H:i:s");

                if ($model->validate()) {
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->CATEGORY_ID]);
                    }
                }
                //Show error message.
                $errors = [];
                foreach ($model->getErrors() as $key => $value) {
                    foreach ($value as $k => $msg) {
                        $errors[] = $msg;
                    }
                }
            }
        }catch (\Exception $exception){
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProductsCategoris model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $model = $this->findModel($id);
            $model->delete();
//            Yii::$app->session->setFlash('success', "Categoris Deleted successfully");
            return $this->redirect(['index']);
        }catch (\Exception $e){
//            Yii::$app->session->setFlash('danger', $e->getMessage());

            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the ProductsCategoris model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductsCategoris the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductsCategoris::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
