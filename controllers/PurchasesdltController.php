<?php

namespace app\controllers;

use Yii;
use app\models\UserPurchasesDtl;
use app\models\PurchasesDtl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PurchasesdltController implements the CRUD actions for UserPurchasesDtl model.
 */
class PurchasesdltController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserPurchasesDtl models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PurchasesDtl();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserPurchasesDtl model.
     * @param integer $PURCHASE_ID
     * @param integer $PRODUCT_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($PURCHASE_ID, $PRODUCT_ID)
    {
        return $this->render('view', [
            'model' => $this->findModel($PURCHASE_ID, $PRODUCT_ID),
        ]);
    }

    /**
     * Creates a new UserPurchasesDtl model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserPurchasesDtl();

        try {
            if (isset($_POST) && !empty($_POST) && isset($_POST['UserPurchasesDtl']) && !empty($_POST['UserPurchasesDtl'])) {

                $UserPurchasesDtl = $_POST['UserPurchasesDtl'];
                $model->attributes = $UserPurchasesDtl;
                $model->CREATED_DATE = date("Y-m-d H:i:s");
                $model->UPDATED_DATE = "0000-00-00 00:00:00";

                if ($model->validate()) {
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->PURCHASE_ID]);
                    }
                }
                //Show error message.
                $errors = [];
                foreach ($model->getErrors() as $key => $value) {
                    foreach ($value as $k => $msg) {
                        $errors[] = $msg;
                    }
                }

            }
        }catch (\Exception $exception){
            return $this->render('create', [
                'model' => $model,
            ]);
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UserPurchasesDtl model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $PURCHASE_ID
     * @param integer $PRODUCT_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($PURCHASE_ID, $PRODUCT_ID)
    {
        $model = $this->findModel($PURCHASE_ID, $PRODUCT_ID);

        try {
            if (isset($_POST) && !empty($_POST) && isset($_POST['UserPurchasesDtl']) && !empty($_POST['UserPurchasesDtl'])) {

                $UserPurchasesDtl = $_POST['UserPurchasesDtl'];
                $model->attributes = $UserPurchasesDtl;
                $model->UPDATED_DATE = date("Y-m-d H:i:s");

                if ($model->validate()) {
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->PURCHASE_ID]);
                    }
                }
                //Show error message.
                $errors = [];
                foreach ($model->getErrors() as $key => $value) {
                    foreach ($value as $k => $msg) {
                        $errors[] = $msg;
                    }
                }

            }
        }catch (\Exception $exception){
            return $this->render('create', [
                'model' => $model,
            ]);
        }


        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UserPurchasesDtl model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $PURCHASE_ID
     * @param integer $PRODUCT_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($PURCHASE_ID, $PRODUCT_ID)
    {
        $this->findModel($PURCHASE_ID, $PRODUCT_ID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserPurchasesDtl model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $PURCHASE_ID
     * @param integer $PRODUCT_ID
     * @return UserPurchasesDtl the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($PURCHASE_ID, $PRODUCT_ID)
    {
        if (($model = UserPurchasesDtl::findOne(['PURCHASE_ID' => $PURCHASE_ID, 'PRODUCT_ID' => $PRODUCT_ID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
