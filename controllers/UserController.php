<?php

namespace app\controllers;

use Yii;
use app\models\UserAccount;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for UserAccount model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserAccount models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new User();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLoginUser()
    {

        $session = Yii::$app->session;
        $user_id = $session['user_id'];

        if(!empty($user_id)){
            return $this->redirect('/');

        }else if (!empty($_POST)){

            if(isset($_POST['username']) && isset($_POST['password'])){
                $user = UserAccount::find()->where(['USER_NAME' => $_POST['username']])->asArray()->one();
                if(!empty($user)){
                    if(md5($_POST['password']) === $user['PASSCODE']){
                        $session['user_id'] = $user['USER_ID'];
                        $session['user_name'] = $user['USER_NAME'];
                        $session['email'] = $user['EMAIL_ADDRESS'];
                        $session['address'] = $user['ADDRESS'];
                        $session['phone'] = $user['PHONE_NUM'];

                        return $this->redirect((!empty(Yii::$app->request->referrer)) ?Yii::$app->request->referrer: Yii::$app->homeUrl);

                    }else{
                        Yii::$app->session->setFlash('danger', Yii::t('app', "incorrect password"));
                        return $this->render('login',['message' => 'incorrect password']);
                    }
                }

            }

        }


        return $this->render('login');
    }

    public function actionRegister()
    {

        return $this->render('register');
    }
    public function actionRegisterData()
    {
        if(!empty($_POST)){
            $model = new UserAccount();
            $model->USER_NAME = $_POST['username'];
            $model->FULL_NAME = $_POST['full_name'];
            $model->COMPANY_NAME = $_POST['company_name'];
            $model->PHONE_NUM = $_POST['phone_number'];
            $model->ADDRESS = $_POST['address'];
            $model->EMAIL_ADDRESS = $_POST['email'];
            $model->PASSCODE = md5($_POST['password']);
            $model->CREATED_DATE = date("Y-m-d H:i:s");
            $model->UPDATED_DATE = "0000-00-00 00:00:00";



            if ($model->validate()) {
                if ($model->save()) {
                    $session = Yii::$app->session;
                    $session['user_id'] = $model->USER_ID;
                    $session['user_name'] = $model->USER_NAME;
                    $session['email'] = $model->EMAIL_ADDRESS;
                    $session['address'] = $model->ADDRESS;
                    $session['phone'] = $model->PHONE_NUM;
                    return $this->redirect('/');

                }
            }else{
//                var_dump($model->getErrors());die;
                return $this->render('register');

            }



        }
        return $this->redirect('/');
    }
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogoutUser()
    {
        $session = Yii::$app->session;
        $session->destroy();
//        var_dump($session['user_id']);die;
//
        return $this->redirect('/');
    }



    /**
     * Displays a single UserAccount model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserAccount model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserAccount();

        try {
            if (isset($_POST) && !empty($_POST) && isset($_POST['UserAccount']) && !empty($_POST['UserAccount'])) {

                $User = $_POST['UserAccount'];
                $model->attributes = $User;
                $model->CREATED_DATE = date("Y-m-d H:i:s");
                $model->UPDATED_DATE = "0000-00-00 00:00:00";

                if ($model->validate()) {
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->USER_ID]);
                    }
                }
                //Show error message.
            $errors = [];
            foreach ($model->getErrors() as $key => $value) {
                foreach ($value as $k => $msg) {
                    $errors[] = $msg;
                }
            }

            }
        }catch (\Exception $exception){
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);


    }

    /**
     * Updates an existing UserAccount model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        try {
            if (isset($_POST) && !empty($_POST) && isset($_POST['UserAccount']) && !empty($_POST['UserAccount'])) {

                $User = $_POST['UserAccount'];
                $model->attributes = $User;
                $model->UPDATED_DATE = date("Y-m-d H:i:s");

                if ($model->validate()) {
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->USER_ID]);
                    }
                }
                //Show error message.
                $errors = [];
                foreach ($model->getErrors() as $key => $value) {
                    foreach ($value as $k => $msg) {
                        $errors[] = $msg;
                    }
                }

            }
        }catch (\Exception $exception){
            return $this->render('create', [
                'model' => $model,
            ]);
        }


        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UserAccount model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try{
            $this->findModel($id)->delete();

            return $this->redirect(['index']);

        }catch (\Exception $exception){
//            var_dump($exception->getMessage());die;

        }

    }

    /**
     * Finds the UserAccount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserAccount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        if (($model = UserAccount::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


}
