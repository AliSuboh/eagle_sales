<?php

namespace app\controllers;

use Yii;
use app\models\UserBillingInfo;
use app\models\BillingInfo;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BillingController implements the CRUD actions for UserBillingInfo model.
 */
class BillingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserBillingInfo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BillingInfo();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserBillingInfo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserBillingInfo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserBillingInfo();

        try {
            if (isset($_POST) && !empty($_POST) && isset($_POST['UserBillingInfo']) && !empty($_POST['UserBillingInfo'])) {

                $BillingInfo = $_POST['UserBillingInfo'];
                $model->attributes = $BillingInfo;
                $model->CREATED_DATE = date("Y-m-d H:i:s");
                $model->UPDATED_DATE = "0000-00-00 00:00:00";

                if ($model->validate()) {
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->PURCHASE_ID]);
                    }
                }
                //Show error message.
                $errors = [];
                foreach ($model->getErrors() as $key => $value) {
                    foreach ($value as $k => $msg) {
                        $errors[] = $msg;
                    }
                }

            }
        }catch (\Exception $exception){
            return $this->render('create', [
                'model' => $model,
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UserBillingInfo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        try {
            if (isset($_POST) && !empty($_POST) && isset($_POST['UserBillingInfo']) && !empty($_POST['UserBillingInfo'])) {

                $BillingInfo = $_POST['UserBillingInfo'];
                $model->attributes = $BillingInfo;
                $model->UPDATED_DATE = date("Y-m-d H:i:s");

                if ($model->validate()) {
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->PURCHASE_ID]);
                    }
                }
                //Show error message.
                $errors = [];
                foreach ($model->getErrors() as $key => $value) {
                    foreach ($value as $k => $msg) {
                        $errors[] = $msg;
                    }
                }

            }
        }catch (\Exception $exception){
            return $this->render('create', [
                'model' => $model,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UserBillingInfo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserBillingInfo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserBillingInfo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserBillingInfo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
