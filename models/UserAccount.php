<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "USER_ACCOUNT".
 *
 * @property int $USER_ID
 * @property string $USER_NAME
 * @property string $FULL_NAME
 * @property string $COMPANY_NAME
 * @property string $PHONE_NUM
 * @property string $ADDRESS
 * @property string $EMAIL_ADDRESS
 * @property string $CREATED_DATE
 * @property string $UPDATED_DATE
 * @property string $PASSCODE
 *
 * @property USERPURCHASES[] $uSERPURCHASESs
 */
class UserAccount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'USER_ACCOUNT';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['USER_NAME', 'FULL_NAME', 'COMPANY_NAME', 'PHONE_NUM', 'ADDRESS', 'EMAIL_ADDRESS', 'CREATED_DATE', 'PASSCODE'], 'required'],
            [['FULL_NAME', 'COMPANY_NAME', 'ADDRESS'], 'string'],
            [['CREATED_DATE', 'UPDATED_DATE'], 'safe'],
            [['USER_NAME', 'PASSCODE'], 'string', 'max' => 100],
            [['PHONE_NUM'], 'string', 'max' => 50],
            [['EMAIL_ADDRESS'],'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'USER_ID' => 'User Id',
            'USER_NAME' => 'User Name',
            'FULL_NAME' => 'Full Name',
            'COMPANY_NAME' => 'Company Name',
            'PHONE_NUM' => 'Phone Number',
            'ADDRESS' => 'Address',
            'EMAIL_ADDRESS' => 'Email',
            'CREATED_DATE' => 'Created Date',
            'UPDATED_DATE' => 'Updated Date',
            'PASSCODE' => 'Passcode',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSERPURCHASESs()
    {
        return $this->hasMany(UserPurchases::className(), ['USER_ID' => 'USER_ID']);
    }
}
