<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "USER_BILLING_INFO".
 *
 * @property int $PURCHASE_ID
 * @property int $BILL_NUMBER
 * @property int $RETURN_VALUE
 * @property int $PAYMENT_STATUS
 * @property string $BILL_ISSUE_DATE
 * @property string $CREATED_DATE
 * @property string $UPDATED_DATE
 * @property int $GATEWAY_TRANS_ID
 *
 * @property USERPURCHASES $pURCHASE
 */
class UserBillingInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'USER_BILLING_INFO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PURCHASE_ID', 'RETURN_VALUE', 'PAYMENT_STATUS', 'BILL_ISSUE_DATE', 'CREATED_DATE', 'GATEWAY_TRANS_ID'], 'required'],
            [['PURCHASE_ID', 'RETURN_VALUE', 'PAYMENT_STATUS', 'GATEWAY_TRANS_ID'], 'integer'],
            [['BILL_ISSUE_DATE', 'CREATED_DATE', 'UPDATED_DATE'], 'safe'],
            [['PURCHASE_ID'], 'exist', 'skipOnError' => true, 'targetClass' => UserPurchases::className(), 'targetAttribute' => ['PURCHASE_ID' => 'PURCHASE_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PURCHASE_ID' => 'Purchase Id',
            'BILL_NUMBER' => 'Bill Number',
            'RETURN_VALUE' => 'Return Value',
            'PAYMENT_STATUS' => 'Payment Status',
            'BILL_ISSUE_DATE' => 'Bill Issue Date',
            'CREATED_DATE' => 'Created Date',
            'UPDATED_DATE' => 'Updated Date',
            'GATEWAY_TRANS_ID' => 'Gatway Transaction Id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPURCHASE()
    {
        return $this->hasOne(UserPurchases::className(), ['PURCHASE_ID' => 'PURCHASE_ID']);
    }
}
