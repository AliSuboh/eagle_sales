<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Products;

/**
 * AddProducts represents the model behind the search form of `app\models\Products`.
 */
class AddProducts extends Products
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PRODUCT_ID', 'CATEGORY_ID'], 'integer'],
            [['PRODUCT_NAME_AR', 'PRODUCT_NAME_EN', 'PRODUCT_DESCRIPTION', 'PRODUCT_DESCRIPTION_EN', 'FILE_PATH', 'SPECIFICATIONS', 'PICTURE', 'CREATED_DATE', 'UPDATED_DATE'], 'safe'],
            [['PRODUCT_PRICE', 'DISCOUNT'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'PRODUCT_ID' => $this->PRODUCT_ID,
            'CATEGORY_ID' => $this->CATEGORY_ID,
            'PRODUCT_PRICE' => $this->PRODUCT_PRICE,
            'DISCOUNT' => $this->DISCOUNT,
            'CREATED_DATE' => $this->CREATED_DATE,
            'UPDATED_DATE' => $this->UPDATED_DATE,
        ]);

        $query->andFilterWhere(['like', 'PRODUCT_NAME_AR', $this->PRODUCT_NAME_AR])
            ->andFilterWhere(['like', 'PRODUCT_NAME_EN', $this->PRODUCT_NAME_EN])
            ->andFilterWhere(['like', 'PRODUCT_DESCRIPTION', $this->PRODUCT_DESCRIPTION])
            ->andFilterWhere(['like', 'PRODUCT_DESCRIPTION_EN', $this->PRODUCT_DESCRIPTION_EN])
            ->andFilterWhere(['like', 'FILE_PATH', $this->FILE_PATH])
            ->andFilterWhere(['like', 'SPECIFICATIONS', $this->SPECIFICATIONS])
            ->andFilterWhere(['like', 'PICTURE', $this->PICTURE]);

        return $dataProvider;
    }
}
