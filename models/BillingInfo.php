<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserBillingInfo;

/**
 * BillingInfo represents the model behind the search form of `app\models\UserBillingInfo`.
 */
class BillingInfo extends UserBillingInfo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PURCHASE_ID', 'BILL_NUMBER', 'RETURN_VALUE', 'PAYMENT_STATUS', 'GATEWAY_TRANS_ID'], 'integer'],
            [['BILL_ISSUE_DATE', 'CREATED_DATE', 'UPDATED_DATE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserBillingInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'PURCHASE_ID' => $this->PURCHASE_ID,
            'BILL_NUMBER' => $this->BILL_NUMBER,
            'RETURN_VALUE' => $this->RETURN_VALUE,
            'PAYMENT_STATUS' => $this->PAYMENT_STATUS,
            'BILL_ISSUE_DATE' => $this->BILL_ISSUE_DATE,
            'CREATED_DATE' => $this->CREATED_DATE,
            'UPDATED_DATE' => $this->UPDATED_DATE,
            'GATEWAY_TRANS_ID' => $this->GATEWAY_TRANS_ID,
        ]);

        return $dataProvider;
    }
}
