<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductsCategoris;

/**
 * AddProductsCategoris represents the model behind the search form of `app\models\ProductsCategoris`.
 */
class AddProductsCategoris extends ProductsCategoris
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CATEGORY_ID', 'CATEGORY_TYPE'], 'integer'],
            [['CATEGORY_NAME_AR', 'CATEGORY_NAME_EN', 'DESCRIPTION_AR', 'DESCRIPTION_EN', 'CREATED_DATE','PICTURE', 'UPDATED_DATE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductsCategoris::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'CATEGORY_ID' => $this->CATEGORY_ID,
            'CREATED_DATE' => $this->CREATED_DATE,
            'UPDATED_DATE' => $this->UPDATED_DATE,
            'CATEGORY_TYPE' => $this->CATEGORY_TYPE,
        ]);

        $query->andFilterWhere(['like', 'CATEGORY_NAME_AR', $this->CATEGORY_NAME_AR])
            ->andFilterWhere(['like', 'CATEGORY_NAME_EN', $this->CATEGORY_NAME_EN])
            ->andFilterWhere(['like', 'DESCRIPTION_AR', $this->DESCRIPTION_AR])
            ->andFilterWhere(['like', 'DESCRIPTION_EN', $this->DESCRIPTION_EN])
            ->andFilterWhere(['like', 'PICTURE', $this->PICTURE]);
        return $dataProvider;
    }
}
