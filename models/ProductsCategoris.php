<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "PRODUCTS_CATEGORIES".
 *
 * @property int $CATEGORY_ID
 * @property string $CATEGORY_NAME_AR
 * @property string $CATEGORY_NAME_EN
 * @property string $DESCRIPTION_AR
 * @property string $DESCRIPTION_EN
 * @property string $CREATED_DATE
 * @property string $UPDATED_DATE
 * @property int $CATEGORY_TYPE
 * @property int $PICTURE
 *
 * @property PRODUCTS[] $pRODUCTSs
 */
class ProductsCategoris extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'PRODUCTS_CATEGORIES';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CATEGORY_NAME_AR', 'CATEGORY_NAME_EN', 'DESCRIPTION_AR', 'DESCRIPTION_EN', 'CREATED_DATE', 'UPDATED_DATE', 'CATEGORY_TYPE','PICTURE'], 'required'],
            [['DESCRIPTION_AR', 'DESCRIPTION_EN','PICTURE'], 'string'],
            [['CREATED_DATE', 'UPDATED_DATE'], 'safe'],
            [['CATEGORY_TYPE'], 'integer'],
            [['CATEGORY_NAME_AR', 'CATEGORY_NAME_EN'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CATEGORY_ID' => 'Category Id',
            'CATEGORY_NAME_AR' => 'Category Name AR',
            'CATEGORY_NAME_EN' => 'Category Name EN',
            'DESCRIPTION_AR' => 'Description AR',
            'DESCRIPTION_EN' => 'Description EN',
            'CREATED_DATE' => 'Created Date',
            'UPDATED_DATE' => 'Updated Date',
            'CATEGORY_TYPE' => 'Category Type',
            'PICTURE' => 'Picture',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPRODUCTSs()
    {
        return $this->hasMany(Products::className(), ['CATEGORY_ID' => 'CATEGORY_ID']);
    }
}
