<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 08/03/19
 * Time: 23:12
 */

namespace app\models;

use Yii;
use yii\base\Model;

class Payment extends Model
{
    public $cardname;
    public $cardnumber;
    public $expmonth;
    public $expyear;
    public $cvv;
    public $city;
    public $adr;
    public $email;


    public function rules()
    {
        return [
            [['cardname', 'cardnumber', 'expmonth', 'expyear', 'cvv'], 'required'],
            [['cardnumber', 'expmonth', 'expyear', 'cvv'], 'integer'],
            [['cardname', 'city', 'adr', 'email'], 'string', 'max' => 255],
            ['cvv', 'checkmaxandminvalues'],
            ['expyear', 'checkmaxandminvalues'],
            ['expmonth', 'checkmaxandminvalues'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'cardname' => 'Name on Card',
            'cardnumber' => 'Credit card number',
            'expmonth' => 'Exp Month',
            'expyear' => 'Exp Year',
            'cvv' => 'CVV',
            'city' => 'City',
            'adr' => 'Address',
            'email' => 'Email',

        ];
    }


    public function callGetway()
    {
        echo '<pre>';
        var_dump('callGetway');
        die;

        return ['status' => true];
    }
    public function checkmaxandminvalues($attributeName)
    {
        $attributeList = [
            "cardname"=>['max' => 55,'min' => 2],
            "cardnumber"=>['max' => 16,'min' => 16],
            "expmonth"=>['max' => 2,'min' => 2],
            "expyear"=>['max' => 4,'min' => 4],
            "cvv"=>['max' => 3,'min' => 3],
            "city"=>['max' => 30,'min' => 2],
            "adr"=>['max' => 255,'min' => 2],
            "email"=>['max' => 100,'min' => 6]
    ];

        $maxMin= $attributeList[$attributeName];

        //Here you do your validation logic
        if(strlen($this->$attributeName)  > $maxMin['max'])
        {
            $this->addError($attributeName, "$attributeName  should not be greater than ".$maxMin['max']);
        }
        else if(strlen($this->$attributeName)  < $maxMin['min'])
        {
            $this->addError($attributeName, "$attributeName  should not be less than ".$maxMin['max']);
        }
    }

}