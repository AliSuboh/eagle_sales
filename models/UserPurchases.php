<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "USER_PURCHASES".
 *
 * @property int $PURCHASE_ID
 * @property int $USER_ID
 * @property string $PURCHASE_DATE
 * @property double $TOTAL_PAYMENT
 * @property int $PAYMENT_METHOD
 * @property int $PAYMENT_STATUS
 * @property string $CREATED_DATE
 * @property string $UPDATED_DATE
 * @property string $NOTES
 * @property string $FEATURES
 *
 * @property USERBILLINGINFO[] $uSERBILLINGINFOs
 * @property USERACCOUNT $uSER
 * @property USERPURCHASESDTL[] $uSERPURCHASESDTLs
 * @property PRODUCTS[] $pRODUCTs
 */
class UserPurchases extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'USER_PURCHASES';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['USER_ID', 'PURCHASE_DATE', 'TOTAL_PAYMENT', 'PAYMENT_METHOD', 'PAYMENT_STATUS', 'CREATED_DATE', 'UPDATED_DATE', 'NOTES', 'FEATURES'], 'required'],
            [['USER_ID', 'PAYMENT_METHOD', 'PAYMENT_STATUS'], 'integer'],
            [['PURCHASE_DATE', 'CREATED_DATE', 'UPDATED_DATE'], 'safe'],
            [['TOTAL_PAYMENT'], 'number'],
            [['NOTES', 'FEATURES'], 'string'],
            [['USER_ID'], 'exist', 'skipOnError' => true, 'targetClass' => UserAccount::className(), 'targetAttribute' => ['USER_ID' => 'USER_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PURCHASE_ID' => 'Purchase Id',
            'USER_ID' => 'User Id',
            'PURCHASE_DATE' => 'Purchase Date',
            'TOTAL_PAYMENT' => 'Total Payment',
            'PAYMENT_METHOD' => 'Payment Method',
            'PAYMENT_STATUS' => 'Payment Status',
            'CREATED_DATE' => 'Created Date',
            'UPDATED_DATE' => 'Updated Date',
            'NOTES' => 'Notes',
            'FEATURES' => 'Features',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSERBILLINGINFOs()
    {
        return $this->hasMany(UserBillingInfo::className(), ['PURCHASE_ID' => 'PURCHASE_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSER()
    {
        return $this->hasOne(UserAccount::className(), ['USER_ID' => 'USER_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSERPURCHASESDTLs()
    {
        return $this->hasMany(UserPurchasesDtl::className(), ['PURCHASE_ID' => 'PURCHASE_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPRODUCTs()
    {
        return $this->hasMany(Products::className(), ['PRODUCT_ID' => 'PRODUCT_ID'])->viaTable('USER_PURCHASES_DTL', ['PURCHASE_ID' => 'PURCHASE_ID']);
    }
}
