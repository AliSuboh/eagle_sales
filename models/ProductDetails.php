<?php
/**
 * Created by PhpStorm.
 * User: Salah
 * Date: 2/11/2019
 * Time: 3:09 PM
 */

namespace app\models;
use Yii;
/**
 * This is the model class for table "PRODUCTS_CATEGORIES".
 *
 * @property int $ID
 * @property string $AR_DESC
 * @property string $ENG_DESC
 * @property int $PRODUCT_ID
 *
 * @property PRODUCT_DETAILS[] $pPRODUCT_DETAILS
 */
class ProductDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'PRODUCT_DETAILS';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AR_DESC', 'ENG_DESC'], 'required'],
            [['AR_DESC', 'ENG_DESC'], 'string'],

            [['ID','PRODUCT_ID'], 'integer'],
            [['AR_DESC', 'ENG_DESC'], 'string', 'max' => 200]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AR_DESC' => 'Ar Desc',
            'ENG_DESC' => 'Eng Desc',
            'PRODUCT_ID' => 'Product Id',
            'ID' => 'Id'

        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsDETAILSs()
    {
        return $this->hasMany(Products::className(), ['ID' => 'ID']);
    }

}