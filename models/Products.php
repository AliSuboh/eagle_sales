<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "PRODUCTS".
 *
 * @property int $PRODUCT_ID
 * @property int $CATEGORY_ID
 * @property string $PRODUCT_NAME_AR
 * @property string $PRODUCT_NAME_EN
 * @property string $PRODUCT_DESCRIPTION
 * @property string $PRODUCT_DESCRIPTION_EN
 * @property double $PRODUCT_PRICE
 * @property string $FILE_PATH
 * @property string $SPECIFICATIONS
 * @property double $DISCOUNT
 * @property resource $PICTURE
 * @property string $CREATED_DATE
 * @property string $UPDATED_DATE
 *
 * @property PRODUCTSCATEGORIES $cATEGORY
 * @property USERPURCHASESDTL[] $uSERPURCHASESDTLs
 * @property USERPURCHASES[] $pURCHASEs
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'PRODUCTS';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CATEGORY_ID', 'PRODUCT_NAME_AR', 'PRODUCT_NAME_EN', 'PRODUCT_DESCRIPTION', 'PRODUCT_DESCRIPTION_EN', 'PRODUCT_PRICE', 'FILE_PATH', 'SPECIFICATIONS', 'CREATED_DATE','PICTURE'], 'required'],
            [['CATEGORY_ID'], 'integer'],
            [['PRODUCT_DESCRIPTION', 'PRODUCT_DESCRIPTION_EN', 'SPECIFICATIONS', 'PICTURE'], 'string'],
            [['PRODUCT_PRICE', 'DISCOUNT'], 'number'],
            [['CREATED_DATE', 'UPDATED_DATE'], 'safe'],
            [['PRODUCT_NAME_AR', 'PRODUCT_NAME_EN'], 'string', 'max' => 200],
            [['FILE_PATH'], 'string', 'max' => 1000],
            [['CATEGORY_ID'], 'exist', 'skipOnError' => true, 'targetClass' => ProductsCategoris::className(), 'targetAttribute' => ['CATEGORY_ID' => 'CATEGORY_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PRODUCT_ID' => 'Product Id',
            'CATEGORY_ID' => 'Category Id',
            'PRODUCT_NAME_AR' => 'Product Name AR',
            'PRODUCT_NAME_EN' => 'Product Name EN',
            'PRODUCT_DESCRIPTION' => 'Product Description',
            'PRODUCT_DESCRIPTION_EN' => 'Product Description EN',
            'PRODUCT_PRICE' => 'Product Price',
            'FILE_PATH' => 'File Path',
            'SPECIFICATIONS' => 'Specifications',
            'DISCOUNT' => 'Discount',
            'PICTURE' => 'Picture',
            'CREATED_DATE' => 'Created Date',
            'UPDATED_DATE' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCATEGORY()
    {
        return $this->hasOne(ProductsCategoris::className(), ['CATEGORY_ID' => 'CATEGORY_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSERPURCHASESDTLs()
    {
        return $this->hasMany(UserPurchasesDtl::className(), ['PRODUCT_ID' => 'PRODUCT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPURCHASEs()
    {
        return $this->hasMany(UserPurchases::className(), ['PURCHASE_ID' => 'PURCHASE_ID'])->viaTable('USER_PURCHASES_DTL', ['PRODUCT_ID' => 'PRODUCT_ID']);
    }
}
