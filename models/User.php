<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserAccount;

/**
 * User represents the model behind the search form of `app\models\UserAccount`.
 */
class User extends UserAccount
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['USER_ID'], 'integer'],
            [['USER_NAME', 'FULL_NAME', 'COMPANY_NAME', 'PHONE_NUM', 'ADDRESS', 'EMAIL_ADDRESS', 'CREATED_DATE', 'UPDATED_DATE', 'PASSCODE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserAccount::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'USER_ID' => $this->USER_ID,
            'CREATED_DATE' => $this->CREATED_DATE,
            'UPDATED_DATE' => $this->UPDATED_DATE,
        ]);

        $query->andFilterWhere(['like', 'USER_NAME', $this->USER_NAME])
            ->andFilterWhere(['like', 'FULL_NAME', $this->FULL_NAME])
            ->andFilterWhere(['like', 'COMPANY_NAME', $this->COMPANY_NAME])
            ->andFilterWhere(['like', 'PHONE_NUM', $this->PHONE_NUM])
            ->andFilterWhere(['like', 'ADDRESS', $this->ADDRESS])
            ->andFilterWhere(['like', 'EMAIL_ADDRESS', $this->EMAIL_ADDRESS])
            ->andFilterWhere(['like', 'PASSCODE', $this->PASSCODE]);

        return $dataProvider;
    }
}
