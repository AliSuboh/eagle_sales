<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserPurchasesDtl;

/**
 * PurchasesDtl represents the model behind the search form of `app\models\UserPurchasesDtl`.
 */
class PurchasesDtl extends UserPurchasesDtl
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PURCHASE_ID', 'PRODUCT_ID', 'AMOUNT'], 'integer'],
            [['PRODUCT_PRICE'], 'number'],
            [['CREATED_DATE', 'UPDATED_DATE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserPurchasesDtl::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'PURCHASE_ID' => $this->PURCHASE_ID,
            'PRODUCT_ID' => $this->PRODUCT_ID,
            'PRODUCT_PRICE' => $this->PRODUCT_PRICE,
            'CREATED_DATE' => $this->CREATED_DATE,
            'UPDATED_DATE' => $this->UPDATED_DATE,
            'AMOUNT' => $this->AMOUNT,
        ]);

        return $dataProvider;
    }
}
