<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "USER_PURCHASES_DTL".
 *
 * @property int $PURCHASE_ID
 * @property int $PRODUCT_ID
 * @property double $PRODUCT_PRICE
 * @property string $CREATED_DATE
 * @property string $UPDATED_DATE
 * @property int $AMOUNT
 *
 * @property USERPURCHASES $pURCHASE
 * @property PRODUCTS $pRODUCT
 */
class UserPurchasesDtl extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'USER_PURCHASES_DTL';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PURCHASE_ID', 'PRODUCT_ID', 'PRODUCT_PRICE', 'CREATED_DATE', 'UPDATED_DATE', 'AMOUNT'], 'required'],
            [['PURCHASE_ID', 'PRODUCT_ID', 'AMOUNT'], 'integer'],
            [['PRODUCT_PRICE'], 'number'],
            [['CREATED_DATE', 'UPDATED_DATE'], 'safe'],
            [['PURCHASE_ID', 'PRODUCT_ID'], 'unique', 'targetAttribute' => ['PURCHASE_ID', 'PRODUCT_ID']],
            [['PURCHASE_ID'], 'exist', 'skipOnError' => true, 'targetClass' => UserPurchases::className(), 'targetAttribute' => ['PURCHASE_ID' => 'PURCHASE_ID']],
            [['PRODUCT_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['PRODUCT_ID' => 'PRODUCT_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PURCHASE_ID' => 'Purchase Id',
            'PRODUCT_ID' => 'Product Id',
            'PRODUCT_PRICE' => 'Product Price',
            'CREATED_DATE' => 'Created Date',
            'UPDATED_DATE' => 'Updated Date',
            'AMOUNT' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPURCHASE()
    {
        return $this->hasOne(UserPurchases::className(), ['PURCHASE_ID' => 'PURCHASE_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPRODUCT()
    {
        return $this->hasOne(Products::className(), ['PRODUCT_ID' => 'PRODUCT_ID']);
    }
}
