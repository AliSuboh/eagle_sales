<?php
/**
 * Created by PhpStorm.
 * User: Salah
 * Date: 2/11/2019
 * Time: 3:09 PM
 */

namespace app\models;
use Yii;
/**
 * This is the model class for table "PRODUCTS_CATEGORIES".
 *
 * @property int $CATEGORY_ID
 * @property string $CATEGORY_NAME_AR
 * @property string $CATEGORY_NAME_EN
 * @property string $DESCRIPTION_AR
 * @property string $DESCRIPTION_EN
 * @property string $CREATED_DATE
 * @property string $UPDATED_DATE
 * @property int $CATEGORY_TYPE
 * @property int $PICTURE
 *
 * @property PRODUCTS[] $pRODUCTSs
 */

class ProductSubDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'PRODUCT_SUB_DETAILS';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DESC_AR', 'DESC_EN'], 'required'],
            [['DESC_AR', 'DESC_EN'], 'string'],

            [['ID','PRODUCT_DTL_ID'], 'integer'],
            [['DESC_AR', 'DESC_EN'], 'string', 'max' => 500]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DESC_AR' => 'Ar Desc',
            'DESC_EN' => 'Eng Desc',
            'PRODUCT_DTL_ID' => 'Product Id',
            'ID' => 'Id'

        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsDETAILSs()
    {
        return $this->hasMany(Products::className(), ['PRODUCT_DTL_ID' => 'PRODUCT_DTL_ID']);
    }
}