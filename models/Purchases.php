<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserPurchases;

/**
 * Purchases represents the model behind the search form of `app\models\UserPurchases`.
 */
class Purchases extends UserPurchases
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PURCHASE_ID', 'USER_ID', 'PAYMENT_METHOD', 'PAYMENT_STATUS'], 'integer'],
            [['PURCHASE_DATE', 'CREATED_DATE', 'UPDATED_DATE', 'NOTES', 'FEATURES'], 'safe'],
            [['TOTAL_PAYMENT'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserPurchases::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'PURCHASE_ID' => $this->PURCHASE_ID,
            'USER_ID' => $this->USER_ID,
            'PURCHASE_DATE' => $this->PURCHASE_DATE,
            'TOTAL_PAYMENT' => $this->TOTAL_PAYMENT,
            'PAYMENT_METHOD' => $this->PAYMENT_METHOD,
            'PAYMENT_STATUS' => $this->PAYMENT_STATUS,
            'CREATED_DATE' => $this->CREATED_DATE,
            'UPDATED_DATE' => $this->UPDATED_DATE,
        ]);

        $query->andFilterWhere(['like', 'NOTES', $this->NOTES])
            ->andFilterWhere(['like', 'FEATURES', $this->FEATURES]);

        return $dataProvider;
    }
}
