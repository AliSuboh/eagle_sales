<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 11/03/19
 * Time: 22:32
 */

namespace app\models;


class Module
{
    public static function discountCalc($cart_bag){
        $dis_value = [];
        $total_discount = $total_amount = 0;
        try {
            foreach ($cart_bag as $key => $value) {
                if (isset($value['DISCOUNT']) && !empty($value['DISCOUNT']) && $value['DISCOUNT'] > 0) {
                    $discountOfPrice = ((int)$value['PRODUCT_PRICE'] * (int)$value['DISCOUNT']) / 100;
                    $AfterDiscount =(int)$value['PRODUCT_PRICE'] - $discountOfPrice;

                    $dis_value [$key] = [
                        'discount' => $value['DISCOUNT'],
                        'Discount_amount' => $discountOfPrice,
                        'amountAfterDiscount' => $AfterDiscount,
                    ];
                    $total_discount += $discountOfPrice;
                    $total_amount += $AfterDiscount;
                } else {

                    $dis_value [$key] = [
                        'discount' => 0,
                        'amountAfterDiscount' => $value['PRODUCT_PRICE'],
                        'Discount_amount' => 0

                    ];
                    $total_amount += $value['PRODUCT_PRICE'];
                }

            }
        }catch (\Exception $e){
            var_dump($e->getMessage(),$e->getLine());die;
        }

        return ['totalAmount' => $total_amount,'totalDiscount' => $total_discount,'discount' => $dis_value];
    }

}