<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AddProducts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'PRODUCT_ID') ?>

    <?= $form->field($model, 'CATEGORY_ID') ?>

    <?= $form->field($model, 'PRODUCT_NAME_AR') ?>

    <?= $form->field($model, 'PRODUCT_NAME_EN') ?>

    <?= $form->field($model, 'PRODUCT_DESCRIPTION') ?>

    <?php // echo $form->field($model, 'PRODUCT_DESCRIPTION_EN') ?>

    <?php // echo $form->field($model, 'PRODUCT_PRICE') ?>

    <?php // echo $form->field($model, 'FILE_PATH') ?>

    <?php // echo $form->field($model, 'SPECIFICATIONS') ?>

    <?php // echo $form->field($model, 'DISCOUNT') ?>

    <?php // echo $form->field($model, 'PICTURE') ?>

    <?php // echo $form->field($model, 'CREATED_DATE') ?>

    <?php // echo $form->field($model, 'UPDATED_DATE') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
