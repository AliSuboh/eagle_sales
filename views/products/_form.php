<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CATEGORY_ID')->textInput() ?>

    <?= $form->field($model, 'PRODUCT_NAME_AR')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PRODUCT_NAME_EN')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PRODUCT_DESCRIPTION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PRODUCT_DESCRIPTION_EN')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PRODUCT_PRICE')->textInput() ?>

    <?= $form->field($model, 'FILE_PATH')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SPECIFICATIONS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'DISCOUNT')->textInput() ?>

    <?= $form->field($model, 'PICTURE')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
