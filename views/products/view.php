<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = $model->PRODUCT_ID;
$this->params['breadcrumbs'][] = ['label' => 'products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->PRODUCT_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->PRODUCT_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'PRODUCT_ID',
            'CATEGORY_ID',
            'PRODUCT_NAME_AR',
            'PRODUCT_NAME_EN',
            'PRODUCT_DESCRIPTION:ntext',
            'PRODUCT_DESCRIPTION_EN:ntext',
            'PRODUCT_PRICE',
            'FILE_PATH',
            'SPECIFICATIONS:ntext',
            'DISCOUNT',
            'PICTURE',
            'CREATED_DATE',
            'UPDATED_DATE',
        ],
    ]) ?>

</div>
