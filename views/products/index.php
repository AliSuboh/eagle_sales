<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AddProducts */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Products', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'PRODUCT_ID',
            'CATEGORY_ID',
            'PRODUCT_NAME_AR',
            'PRODUCT_NAME_EN',
            'PRODUCT_DESCRIPTION:ntext',
            //'PRODUCT_DESCRIPTION_EN:ntext',
            //'PRODUCT_PRICE',
            //'FILE_PATH',
            //'SPECIFICATIONS:ntext',
            //'DISCOUNT',
            //'PICTURE',
            //'CREATED_DATE',
            //'UPDATED_DATE',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
