<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Accounts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-account-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Account', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'USER_ID',
            'USER_NAME',
            'FULL_NAME:ntext',
            'COMPANY_NAME:ntext',
            'PHONE_NUM',
            //'ADDRESS:ntext',
            //'EMAIL_ADDRESS:email',
            //'CREATED_DATE',
            //'UPDATED_DATE',
            //'PASSCODE',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
