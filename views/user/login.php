

<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- Page Title -->
<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Login with Social Media or Manually</h1>
            </div>
        </div>
    </div>
</div>
<div class="container_log">
    <form class="form" action="" id="registerForm" accept-charset="utf-8" method="post" autocomplete='off'>
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
        <div class="row_log">

            <div class="vl">
                <span class="vl-innertext">or</span>
            </div>

            <div class="col_log">
                <a href="#" class="fb btn_log">
                    <i class="fa fa-facebook fa-fw"></i> Login with Facebook
                </a>
                <a href="#" class="twitter btn_log">
                    <i class="fa fa-twitter fa-fw"></i> Login with Twitter
                </a>
                <a href="#" class="google btn_log"><i class="fa fa-google fa-fw">
                    </i> Login with Google+
                </a>
            </div>

            <div class="col_log">
                <div class="hide-md-lg">
                    <p>Or sign in manually:</p>
                </div>

                <input class="form-control" id="login-username" name="username" type="text" placeholder="Username" required>
                <input class="form-control" id="login-password" name="password" type="password" placeholder="Password" required>
                <input type="submit" class ="btn_log" value="Login">
            </div>

        </div>
    </form>
</div>

<div class="bottom-container_log">
    <div class="row_log">
        <div class="col_log">
            <a href="/register" style="color:white" class="btn_log">Register</a>
        </div>
        <div class="col_log">
            <a  href="/password-reset" style="color:white" class="btn_log">Forgot password ?</a>
        </div>
    </div>
