<!-- Page Title -->
<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Register</h1>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="basic-login">
                    <form class="form" action="/registerData" id="registerForm" accept-charset="utf-8" method="post" autocomplete='off'>
                        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                        <div class="form-group" >
                            <div class="col-sm-6">
                            <label for="username"><i class="icon-user"></i> <b>User Name</b></label>
                            <input class="form-control" id="username" name="username" type="text" placeholder="please add user name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                            <label for="full_name"><i class="icon-lock"></i> <b>Full Name</b></label>
                            <input class="form-control" id="full_name" name="full_name" type="text" placeholder="please add your Full Name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                            <label for="company_name"><i class="icon-user"></i> <b>Company Name</b></label>
                            <input class="form-control" id="company_name" name="company_name" type="text" placeholder="please add your Company Name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                            <label for="phone_number"><i class="icon-user"></i> <b>Phone Number</b></label>
                            <input class="form-control" id="phone_number" name="phone_number" type="text" placeholder="please add your Phone Number" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                            <label for="address"><i class="icon-lock"></i> <b>Address Line</b></label>
                            <input class="form-control" id="address" type="text" name="address" placeholder="please add your address" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6">
                            <label for="email"><i class="icon-lock"></i> <b>Email</b></label>
                            <input class="form-control" id="email" name="email" type="text" placeholder="please add your email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                            <label for="password"><i class="icon-lock"></i> <b>Password</b></label>
                            <input class="form-control" id="password" name="password" type="password" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                            <label for="re_password"><i class="icon-lock"></i> <b>Re-enter Password</b></label>
                            <input class="form-control" id="re_password" name="re_password" type="password" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="clearfix"></div>
                        </div>
                        <div class="form-actions" style="padding-left: 35%">
                            <p><input type="checkbox" required name="terms"> I accept the <u>Terms and Conditions</u></p>
                            <button type="submit" class="btn btn-rounded btn-primary btn-lg smooth-scroll validate">Register</button>
                            <a class="btn btn-rounded btn-default btn-lg smooth-scroll cancel" href="/" title="Cancel">Cancel</a>
                            <div class="clearfix"></div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>