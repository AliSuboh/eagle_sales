<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-account-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'USER_ID') ?>

    <?= $form->field($model, 'USER_NAME') ?>

    <?= $form->field($model, 'FULL_NAME') ?>

    <?= $form->field($model, 'COMPANY_NAME') ?>

    <?= $form->field($model, 'PHONE_NUM') ?>

    <?php // echo $form->field($model, 'ADDRESS') ?>

    <?php // echo $form->field($model, 'EMAIL_ADDRESS') ?>

    <?php // echo $form->field($model, 'CREATED_DATE') ?>

    <?php // echo $form->field($model, 'UPDATED_DATE') ?>

    <?php // echo $form->field($model, 'PASSCODE') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
