<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

$session = Yii::$app->session;
$user_id = $session['user_id'];
$user_name = $session['user_name'];
$cart_list =($session['cart_list'])?$session['cart_list']:[];
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <title>mPurpose - Multipurpose Feature Rich Bootstrap Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap">
    <!-- Navigation & Logo-->
    <div class="mainmenu-wrapper">
        <div class="container">
            <div class="menuextras">
                <div class="extras">
                    <ul>
                        <li class="shopping-carFeaturest-items"><i class="glyphicon glyphicon-shopping-cart icon-white" name="Items_cart" id="Items_cart"></i> <a name="Items_cart_COUNT" id="Items_cart_COUNT" href="/shopping-cart"><b><?=count($cart_list)?> items</b></a></li>
                        <li>
                            <div class="dropdown choose-country">
                                <a class="#" data-toggle="dropdown" href="#"><img src="/img/flags/gb.png" alt="Great Britain"> UK</a>
                                <ul class="dropdown-menu" role="menu">
                                    <li role="menuitem"><a href="#"><img src="/img/flags/us.png" alt="United States"> US</a></li>
                                    <li role="menuitem"><a href="#"><img src="/img/flags/de.png" alt="Germany"> DE</a></li>
                                    <li role="menuitem"><a href="#"><img src="/img/flags/es.png" alt="Spain"> ES</a></li>
                                </ul>
                            </div>
                        </li>
                        <?php if(!empty($user_id)){ ?>
                        <li><samp>Welcome <?=$user_name?>  </samp><a href="/logout-user">  Logout</a></li>
                        <?php }else{ ?>
                            <li><a href="/login-user">Login</a></li>

                        <?php } ?>
                    </ul>
                </div>
            </div>
            <nav id="mainmenu" class="mainmenu">
                <ul>
                    <li class="logo-wrapper"><a href="/"><img src="/img/mPurpose-logo.png" alt="Multipurpose Twitter Bootstrap Template"></a></li>
                    <li   <?php if($_SERVER['REQUEST_URI'] === "/") echo 'class="active"'; ?>>
                        <a href="/">Home</a>
                    </li>
                    <li <?php if(strpos(strtolower($_SERVER['REQUEST_URI']),"products") !== false) echo 'class="active"'; ?>>
                        <a href="/Products">Products</a>
                    </li>
                    <li class="has-submenu">
                        <a href="#">Pages +</a>
                        <div class="mainmenu-submenu">
                            <div class="mainmenu-submenu-inner">
                                <div>
                                    <h4>Homepage</h4>
                                    <ul>
                                        <li><a href="/">Homepage </a></li>

                                    </ul>
<!--                                    <h4>Services & Pricing</h4>-->
<!--                                    <ul>-->
<!--                                        <li><a href="page-services-1-column.html">System solutions and design</a></li>-->
<!--                                        <li><a href="page-services-1-column.html">ORACLE Support and DBA </a></li>-->
<!--                                        <li><a href="page-services-3-columns.html">Computers maintenance and technical support</a></li>-->
<!--                                        <li><a href="page-services-4-columns.html">Web Design and hosting services </a></li>-->
<!--                                        <li><a href="page-pricing.html">Pricing Table</a></li>-->
<!--                                    </ul>-->
                                    <h4>Team & Open Vacancies</h4>
                                    <ul>
                                        <li><a href="page-team.html">Our Team</a></li>
                                        <li><a href="page-vacancies.html">Open Vacancies (List)</a></li>
                                        <li><a href="page-job-details.html">Open Vacancies (Job Details)</a></li>
                                    </ul>
                                </div>

                                <div>

                                    <h4>General Pages</h4>
                                    <ul>
                                        <li><a href="/About">About Us</a></li>
                                        <li><a href="/Contact-Us">Contact Us</a></li>
                                        <li><a href="page-faq.html">Frequently Asked Questions</a></li>

                                        <li><a href="/login-user">Login</a></li>
                                        <li><a href="/register">Register</a></li>
                                        <li><a href="/password-reset">Password Reset</a></li>
                                        <li><a href="page-terms-privacy.html">Terms & Privacy</a></li>

                                    </ul>
                                </div>

                            </div><!-- /mainmenu-submenu-inner -->
                        </div><!-- /mainmenu-submenu -->
                    </li>
                    <li><a href="/About">About Us</a></li>

                </ul>
            </nav>
        </div>
    </div>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>

    </div>
</div>

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-footer col-md-3 col-xs-6">
                <h3>Our Latest Work</h3>
                <div class="portfolio-item">
                    <div class="portfolio-image">
                        <a href="page-portfolio-item.html"><img src="/img/portfolio6.jpg" alt="Project Name"></a>
                    </div>
                </div>
            </div>
            <div class="col-footer col-md-3 col-xs-6">
                <h3>Navigate</h3>
                <ul class="no-list-style footer-navigate-section">
                    <li><a href="page-blog-posts.html">Blog</a></li>
                    <li><a href="page-portfolio-3-columns-2.html">Portfolio</a></li>
                    <li><a href="page-products-3-columns.html">eShop</a></li>
                    <li><a href="page-services-3-columns.html">Services</a></li>
                    <li><a href="page-pricing.html">Pricing</a></li>
                    <li><a href="page-faq.html">FAQ</a></li>
                </ul>
            </div>

            <div class="col-footer col-md-4 col-xs-6">
                <h3>Contacts</h3>
                <p class="contact-us-details">
                    <b>Address:</b> Jordan- Amman <br/>
                    <b>Phone:</b>   +962 787171848<br/>
                    <b>Fax:</b>  +962 787171848<br/>
                    <b>Email:</b> <a href="Salah.as72@gmail.com"> Salah.as72@gmail.com </a>
                </p>
            </div>
            <div class="col-footer col-md-2 col-xs-6">
                <h3>Stay Connected</h3>
                <ul class="footer-stay-connected no-list-style">
                    <li><a href="#" class="facebook"></a></li>
                    <li><a href="#" class="twitter"></a></li>
                    <li><a href="#" class="googleplus"></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="footer-copyright">&copy; ©2012-2017 Eagle company . All rights reserved.</div>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
