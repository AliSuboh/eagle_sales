<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductsCategoris */

$this->title = 'Create Products Categoris';
$this->params['breadcrumbs'][] = ['label' => 'Products Categoris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-categoris-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
