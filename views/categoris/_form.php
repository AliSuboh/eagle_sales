<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductsCategoris */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-categoris-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CATEGORY_NAME_AR')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CATEGORY_NAME_EN')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DESCRIPTION_AR')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'DESCRIPTION_EN')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'CATEGORY_TYPE')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
