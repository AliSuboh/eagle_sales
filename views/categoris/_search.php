<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AddProductsCategoris */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-categoris-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'CATEGORY_ID') ?>

    <?= $form->field($model, 'CATEGORY_NAME_AR') ?>

    <?= $form->field($model, 'CATEGORY_NAME_EN') ?>

    <?= $form->field($model, 'DESCRIPTION_AR') ?>

    <?= $form->field($model, 'DESCRIPTION_EN') ?>

    <?php // echo $form->field($model, 'CREATED_DATE') ?>

    <?php // echo $form->field($model, 'UPDATED_DATE') ?>

    <?php // echo $form->field($model, 'CATEGORY_TYPE') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
