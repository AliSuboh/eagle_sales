<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductsCategoris */

$this->title = 'Update Products Categoris: ' . $model->CATEGORY_ID;
$this->params['breadcrumbs'][] = ['label' => 'Products Categoris', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CATEGORY_ID, 'url' => ['view', 'id' => $model->CATEGORY_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="products-categoris-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
