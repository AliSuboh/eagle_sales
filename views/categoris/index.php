<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AddProductsCategoris */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products Categoris';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-categoris-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Products Categoris', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'CATEGORY_ID',
            'CATEGORY_NAME_AR',
            'CATEGORY_NAME_EN',
            'DESCRIPTION_AR:ntext',
            'DESCRIPTION_EN:ntext',
            //'CREATED_DATE',
            //'UPDATED_DATE',
            //'CATEGORY_TYPE',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
