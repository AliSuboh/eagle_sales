<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserBillingInfo */

$this->title = 'Create User Billing Info';
$this->params['breadcrumbs'][] = ['label' => 'User Billing Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-billing-info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
