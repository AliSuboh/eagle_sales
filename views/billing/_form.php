<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserBillingInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-billing-info-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'PURCHASE_ID')->textInput() ?>

    <?= $form->field($model, 'RETURN_VALUE')->textInput() ?>

    <?= $form->field($model, 'PAYMENT_STATUS')->textInput() ?>

    <?= $form->field($model, 'BILL_ISSUE_DATE')->textInput() ?>

    <?= $form->field($model, 'GATEWAY_TRANS_ID')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
