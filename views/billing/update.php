<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserBillingInfo */

$this->title = 'Update User Billing Info: ' . $model->BILL_NUMBER;
$this->params['breadcrumbs'][] = ['label' => 'User Billing Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->BILL_NUMBER, 'url' => ['view', 'id' => $model->BILL_NUMBER]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-billing-info-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
