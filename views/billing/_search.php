<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BillingInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-billing-info-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'PURCHASE_ID') ?>

    <?= $form->field($model, 'BILL_NUMBER') ?>

    <?= $form->field($model, 'RETURN_VALUE') ?>

    <?= $form->field($model, 'PAYMENT_STATUS') ?>

    <?= $form->field($model, 'BILL_ISSUE_DATE') ?>

    <?php // echo $form->field($model, 'CREATED_DATE') ?>

    <?php // echo $form->field($model, 'UPDATED_DATE') ?>

    <?php // echo $form->field($model, 'GATEWAY_TRANS_ID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
