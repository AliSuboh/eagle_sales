<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserBillingInfo */

$this->title = $model->BILL_NUMBER;
$this->params['breadcrumbs'][] = ['label' => 'User Billing Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-billing-info-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->BILL_NUMBER], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->BILL_NUMBER], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'PURCHASE_ID',
            'BILL_NUMBER',
            'RETURN_VALUE',
            'PAYMENT_STATUS',
            'BILL_ISSUE_DATE',
            'CREATED_DATE',
            'UPDATED_DATE',
            'GATEWAY_TRANS_ID',
        ],
    ]) ?>

</div>
