<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BillingInfo */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Billing Infos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-billing-info-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Billing Info', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'PURCHASE_ID',
            'BILL_NUMBER',
            'RETURN_VALUE',
            'PAYMENT_STATUS',
            'BILL_ISSUE_DATE',
            //'CREATED_DATE',
            //'UPDATED_DATE',
            //'GATEWAY_TRANS_ID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
