<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserPurchases */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-purchases-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'USER_ID')->textInput() ?>

    <?= $form->field($model, 'PURCHASE_DATE')->textInput() ?>

    <?= $form->field($model, 'TOTAL_PAYMENT')->textInput() ?>

    <?= $form->field($model, 'PAYMENT_METHOD')->textInput() ?>

    <?= $form->field($model, 'PAYMENT_STATUS')->textInput() ?>


    <?= $form->field($model, 'NOTES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'FEATURES')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
