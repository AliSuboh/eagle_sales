<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Purchases */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-purchases-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'PURCHASE_ID') ?>

    <?= $form->field($model, 'USER_ID') ?>

    <?= $form->field($model, 'PURCHASE_DATE') ?>

    <?= $form->field($model, 'TOTAL_PAYMENT') ?>

    <?= $form->field($model, 'PAYMENT_METHOD') ?>

    <?php // echo $form->field($model, 'PAYMENT_STATUS') ?>

    <?php // echo $form->field($model, 'CREATED_DATE') ?>

    <?php // echo $form->field($model, 'UPDATED_DATE') ?>

    <?php // echo $form->field($model, 'NOTES') ?>

    <?php // echo $form->field($model, 'FEATURES') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
