<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserPurchases */

$this->title = 'Create User Purchases';
$this->params['breadcrumbs'][] = ['label' => 'User Purchases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-purchases-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
