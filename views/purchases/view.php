<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserPurchases */

$this->title = $model->PURCHASE_ID;
$this->params['breadcrumbs'][] = ['label' => 'User Purchases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-purchases-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->PURCHASE_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->PURCHASE_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'PURCHASE_ID',
            'USER_ID',
            'PURCHASE_DATE',
            'TOTAL_PAYMENT',
            'PAYMENT_METHOD',
            'PAYMENT_STATUS',
            'CREATED_DATE',
            'UPDATED_DATE',
            'NOTES:ntext',
            'FEATURES:ntext',
        ],
    ]) ?>

</div>
