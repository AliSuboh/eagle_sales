<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserPurchases */

$this->title = 'Update User Purchases: ' . $model->PURCHASE_ID;
$this->params['breadcrumbs'][] = ['label' => 'User Purchases', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->PURCHASE_ID, 'url' => ['view', 'id' => $model->PURCHASE_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-purchases-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
