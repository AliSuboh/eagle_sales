<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Purchases */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Purchases';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-purchases-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Purchases', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'PURCHASE_ID',
            'USER_ID',
            'PURCHASE_DATE',
            'TOTAL_PAYMENT',
            'PAYMENT_METHOD',
            //'PAYMENT_STATUS',
            //'CREATED_DATE',
            //'UPDATED_DATE',
            //'NOTES:ntext',
            //'FEATURES:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
