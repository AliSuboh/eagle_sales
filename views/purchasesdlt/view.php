<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserPurchasesDtl */

$this->title = $model->PURCHASE_ID;
$this->params['breadcrumbs'][] = ['label' => 'User Purchases Dtls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-purchases-dtl-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'PURCHASE_ID' => $model->PURCHASE_ID, 'PRODUCT_ID' => $model->PRODUCT_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'PURCHASE_ID' => $model->PURCHASE_ID, 'PRODUCT_ID' => $model->PRODUCT_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'PURCHASE_ID',
            'PRODUCT_ID',
            'PRODUCT_PRICE',
            'CREATED_DATE',
            'UPDATED_DATE',
            'AMOUNT',
        ],
    ]) ?>

</div>
