<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PurchasesDtl */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Purchases Dtls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-purchases-dtl-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Purchases Dtl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'PURCHASE_ID',
            'PRODUCT_ID',
            'PRODUCT_PRICE',
            'CREATED_DATE',
            'UPDATED_DATE',
            //'AMOUNT',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
