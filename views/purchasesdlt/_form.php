<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserPurchasesDtl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-purchases-dtl-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'PURCHASE_ID')->textInput() ?>

    <?= $form->field($model, 'PRODUCT_ID')->textInput() ?>

    <?= $form->field($model, 'PRODUCT_PRICE')->textInput() ?>

    <?= $form->field($model, 'AMOUNT')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
