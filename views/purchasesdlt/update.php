<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserPurchasesDtl */

$this->title = 'Update User Purchases Dtl: ' . $model->PURCHASE_ID;
$this->params['breadcrumbs'][] = ['label' => 'User Purchases Dtls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->PURCHASE_ID, 'url' => ['view', 'PURCHASE_ID' => $model->PURCHASE_ID, 'PRODUCT_ID' => $model->PRODUCT_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-purchases-dtl-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
