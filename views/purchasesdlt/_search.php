<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PurchasesDtl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-purchases-dtl-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'PURCHASE_ID') ?>

    <?= $form->field($model, 'PRODUCT_ID') ?>

    <?= $form->field($model, 'PRODUCT_PRICE') ?>

    <?= $form->field($model, 'CREATED_DATE') ?>

    <?= $form->field($model, 'UPDATED_DATE') ?>

    <?php // echo $form->field($model, 'AMOUNT') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
