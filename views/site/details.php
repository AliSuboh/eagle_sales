

<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Details';
$this->params['breadcrumbs'][] = ['label' => 'Details', 'url' => ['/Details']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Product Details</h1>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <!-- Product Image & Available Colors -->
            <div class="col-sm-6">
                <div class="product-image-large">
                    <img src="<?=$product['PICTURE']?>" alt="Item Name">
                </div>

            </div>
            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
            <!-- End Product Image & Available Colors -->
            <!-- Product Summary & Options -->
            <div class="col-sm-6 product-details">
                <h4><?=$product['PRODUCT_NAME_EN']?></h4>
                <div class="price">
                    <?php if($product['DISCOUNT'] > 0){ ?>
                    <span class="price-was">$<?=$product['PRODUCT_PRICE']?></span> <?=($product['PRODUCT_PRICE'] - ($product['PRODUCT_PRICE']*$product['DISCOUNT'])/100)?>
                    <?php }else{ ?>
                        <span >$<?=$product['PRODUCT_PRICE']?></span>

                    <?php } ?>
                </div>
                <h5>Quick Overview</h5>
                <p>
                    <?=$product['PRODUCT_DESCRIPTION_EN']?>
                </p>
                <table class="shop-item-selections">
                    <!-- Color Selector -->
                    <tr>
                        <td><b>Product:</b></td>
                        <td>
                            <div class="dropdown choose-item-color" >
                                <a class="btn btn-sm btn-grey categories disabled" data-toggle="dropdown" name="<?=$categories[0]['CATEGORY_ID']?>"> <?=$categories[0]['CATEGORY_NAME_EN']?> <b class="caret"></b></a>
                                <ul class="dropdown-menu" role="menu" >
                                    <?php foreach ($categories as $category){ ?>
                                    <li role="menuitem" name="<?=$category['CATEGORY_ID']?>"><a ><?=$category['CATEGORY_NAME_EN']?></a></li>

                                    <?php } ?>

                                </ul>
                            </div>
                        </td>
                    </tr>
                    <!-- Size Selector -->
                    <tr>
                        <td><b>Period:</b></td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-sm btn-grey disabled" data-toggle="dropdown" href="#">One Year <b class="caret"></b></a>
                                <ul class="dropdown-menu " role="menu" disabled="true">
                                    <li role="menuitem"><a href="#">One Year</a></li>
                                    <li role="menuitem"><a href="#">Two Years</a></li>
                                    <li role="menuitem"><a href="#">Three Years</a></li>

                                </ul>
                            </div>
                        </td>
                    </tr>
                    <!-- Quantity -->
                    <tr>
                        <td><b>Quantity:</b></td>
                        <td>
                            <select name="quantity" autocomplete="off" id="quantity" tabindex="-1" class="a-native-dropdown disabled">
                                <option value="1" selected="1">1

                                <?php for($i=2 ; $i <= 5 ; $i++){ ?>
                                <option value="<?=$i?>" ><?=$i?>
                                </option>
                                <?php } ?>

                            </select>
                        </td>
                    </tr>
                    <!-- Add to Cart Button -->
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <div class="actions">

                                <?php if(!in_array($product['PRODUCT_ID'],$cart_list)){ ?>
                                    <button  class="btn btn-small product_id product_id_<?=$product['PRODUCT_ID']?>"   value="<?=$product['PRODUCT_ID']?>" name="<?=$product['PRODUCT_ID']?>"><i class="icon-shopping-cart icon-white"></i> Add to Cart</button>
                                <?php }else{ ?>
                                    <button  class="btn btn-small product_id product_id_<?=$product['PRODUCT_ID']?> disabled"   value="<?=$product['PRODUCT_ID']?>" name="<?=$product['PRODUCT_ID']?>"><i class="icon-shopping-cart icon-white"></i> Added</button>
                                <?php }?>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- End Product Summary & Options -->

            <!-- Full Description & Specification -->
            <div class="col-sm-12">
                <div class="tabbable">
                    <!-- Tabs -->
                    <ul class="nav nav-tabs product-details-nav">
                        <li class="active"><a href="#tab1" data-toggle="tab">Description</a></li>
                        <li><a id="to_tap2" href="#tab2" data-toggle="tab">Specification</a></li>
                    </ul>
                    <!-- Tab Content (Full Description) -->
                    <div class="tab-content product-detail-info">
                        <div class="tab-pane active" id="tab1">

                            <h4>Product Description</h4>
                            <p>
                                <?=$product['PRODUCT_DESCRIPTION_EN']?>
                            </p>
                            <h4>Product Highlights</h4>
                            <?php
                            if(isset($product_details))
                            foreach ($product_details     as $key => $product_detail) {?>
                            <ul>
                                <li><?=$product_detail['ENG_DESC']?> <a id="<?=$product_detail['ID']?>" onclick="callSubDetail(<?=$product_detail['ID']?>,'<?=$product_detail['ENG_DESC']?>');">more...</a> </li>
                            </ul>
                            <?php } ?>
                        </div>

                        <!-- Tab Content (Specification) -->
                        <div class="tab-pane" id="tab2">
                            <h4 id="header_title"></h4>
                            <ul id="tbl_specification">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Full Description & Specification -->
        </div>
    </div>
</div>