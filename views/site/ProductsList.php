

<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Products List';
//$this->params['breadcrumbs'][] ="  $path / ". $this->title;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['/Products']];
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Page Title -->
<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?=$this->title?></h1>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
<div class="eshop-section section">
    <div class="container">
        <div class="row">
            <?php foreach ($products as $key => $product){?>
            <div class="col-md-3 col-sm-6">
                <div class="shop-item" >
                    <div class="image">
                        <a href="/Details/<?=$product['PRODUCT_ID']?>"><img src="<?=$product['PICTURE']?>" alt="Item Name"></a>
                    </div>
                    <div class="title">
                        <h3><a href="/Details/<?=$product['PRODUCT_ID']?>"><?=$product['PRODUCT_NAME_EN']?></a></h3>
                    </div>
                    <div class="price">
                        <?=$product['PRODUCT_PRICE']?>$
                    </div>

                    <div class="actions">
                        <?php if(!in_array($product['PRODUCT_ID'],$cart_list)){ ?>
                            <button  class="btn btn-small product_id product_id_<?=$product['PRODUCT_ID']?>"   value="<?=$product['PRODUCT_ID']?>" name="<?=$product['PRODUCT_ID']?>"><i class="icon-shopping-cart icon-white"></i> Add to Cart</button>
                        <?php }else{ ?>
                            <button  class="btn btn-small product_id product_id_<?=$product['PRODUCT_ID']?> disabled"   value="<?=$product['PRODUCT_ID']?>" name="<?=$product['PRODUCT_ID']?>"><i class="icon-shopping-cart icon-white"></i> Added</button>
                        <?php }?> <i class="icon-shopping-cart icon-white"></i></a> <span>or <a href="/Details/<?=$product['PRODUCT_ID']?>" >Read more</a></span>
                    </div>

                </div>
            </div>
            <?php } ?>

        </div>
        <div class="pagination-wrapper ">
            <ul class="pagination pagination-lg">
                <li class="disabled"><a href="#">Previous</a></li>
                <li class="active"><a href="#">1</a></li>

                <?php $total_pages = ceil($products_count / 5);
                for ($i=2; $i<=$total_pages; $i++) { ?>
                    <li><a href="Software?page=<?=$i?>"  value="<?=$i?>"><?=$i?></a></li>

                <?php } ?>
                <li><a href="#">Next</a></li>
            </ul>
        </div>
    </div>
</div>
