
<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'CIOSG';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Homepage Slider -->
<div class="homepage-slider">
    <div id="sequence">
        <ul class="sequence-canvas">
            <!-- Slide 1 -->
            <li class="bg4">
                <!-- Slide Title -->
                <h2 class="title">CIOSG ERP Solution </h2>
                <!-- Slide Text -->
                <h3 class="subtitle"> Bright Ideas for Your Business,, </h3>
                <!-- Slide Image -->
                <img class="slide-img" src="/img/homepage-slider/slide1.png" alt="Slide 1" />
            </li>
            <!-- End Slide 1 -->
            <!-- Slide 2 -->
            <li class="bg3">
                <!-- Slide Title -->
                <h2 class="title">Easy to use</h2>
                <!-- Slide Text -->
                <h3 class="subtitle">Provides Graphical User Interface (GUI) which is simple and easy to use</h3>
                <!-- Slide Image -->
                <img class="slide-img" src="/img/homepage-slider/slide2.png" alt="Slide 2" />
            </li>
            <!-- End Slide 2 -->
            <!-- Slide 3 -->
            <li class="bg1">
                <!-- Slide Title -->
                <h2 class="title">Secure</h2>
                <!-- Slide Text -->
                <h3 class="subtitle">System maintains data (security and privacy) through multilevel
                    authorization procedures.</h3>
                <!-- Slide Image -->
                <img class="slide-img" src="/img/homepage-slider/slide3.png" alt="Slide 3" />
            </li>
            <!-- End Slide 3 -->

        </ul>
        <div class="sequence-pagination-wrapper">
            <ul class="sequence-pagination">
                <li>1</li>
                <li>2</li>
                <li>3</li>
            </ul>
        </div>
    </div>
</div>
<!-- End Homepage Slider -->

<!-- Services -->
<div class="section">
    <div class="container">
        <h2>Our Services</h2>
        <div class="row">
            <div class="col-md-5  col-sm-5">
                <div class="service-wrapper">
                    <img src="img/service-icon/diamond.png" alt="Service Name">
                    <h3>System solutions and design</h3>
                    <ul class="services_li">
                        <li>ERP system </li>
                        <li> Administrative software for Training institution</li>
                        <li> Clinics management system</li>
                        <li>  Small establishments management system</li>
                        <li>   Typing teaching software( Arabic/ English)</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5  col-sm-5">
                <div class="service-wrapper">
                    <img src="img/service-icon/box.png" alt="Service Name">
                    <h3>Computers maintenance and technical support</h3>
                    <ul class="services_li">
                        <li> Computers & laptops maintenance </li>
                        <li>Hosting all types of website( asp.net, php, html )</li>
                        <li>Networking and domains configuration </li>
                        <li> Technical support </li>

                    </ul>
                </div>
            </div>
        </div>
            <div class="row">
            <div class="col-md-5  col-sm-5">
                <div class="service-wrapper">
                    <img src="img/service-icon/ruler.png" alt="Service Name">
                    <h3>Web Design and hosting services</h3>
                    <ul class="services_li">
                        <li>ASP.net and SQLserver 2008</li>
                        <li>Hosting all types of website( asp.net, php, html )</li>
                        <li>Email support</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5  col-sm-5">
                <div class="service-wrapper">
                    <img src="img/service-icon/diamond.png" alt="Service Name">
                    <h3>ORACLE Support and DBA</h3>
                    <ul class="services_li" >
                        <li >Database administration support</li>
                        <li>ORACLE softwares development support</li>
                        <li>Consultations</li>


                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Services -->

<!-- Pricing Table -->
<div class="section">
    <div class="container">
        <h2>Pricing</h2>
        <div class="row">
            <!-- Pricing Plans Wrapper -->
            <div class="pricing-wrapper col-md-12">
                <!-- Pricing Plan -->
                <div class="pricing-plan">
                    <!-- Pricing Plan Ribbon -->
                    <div class="ribbon-wrapper">
                        <div class="price-ribbon ribbon-red">Popular</div>
                    </div>
                    <h2 class="pricing-plan-title">Starter</h2>
                    <p class="pricing-plan-price">FREE</p>
                    <!-- Pricing Plan Features -->
                    <ul class="pricing-plan-features">
                        <li><strong>1</strong> user</li>
                        <li><strong>Unlimited</strong> projects</li>
                        <li><strong>2GB</strong> storage</li>
                    </ul>
                    <a href="index.html" class="btn">Order Now</a>
                </div>
                <!-- End Pricing Plan -->
                <div class="pricing-plan">
                    <h2 class="pricing-plan-title">Advanced</h2>
                    <p class="pricing-plan-price">$49<span>/mo</span></p>
                    <ul class="pricing-plan-features">
                        <li><strong>10</strong> users</li>
                        <li><strong>Unlimited</strong> projects</li>
                        <li><strong>20GB</strong> storage</li>
                    </ul>
                    <a href="index.html" class="btn">Order Now</a>
                </div>
                <!-- Promoted Pricing Plan -->
                <div class="pricing-plan pricing-plan-promote">
                    <h2 class="pricing-plan-title">Premium</h2>
                    <p class="pricing-plan-price">$99<span>/mo</span></p>
                    <ul class="pricing-plan-features">
                        <li><strong>Unlimited</strong> users</li>
                        <li><strong>Unlimited</strong> projects</li>
                        <li><strong>100GB</strong> storage</li>
                    </ul>
                    <a href="index.html" class="btn">Order Now</a>
                </div>
                <div class="pricing-plan">
                    <!-- Pricing Plan Ribbon -->
                    <div class="ribbon-wrapper">
                        <div class="price-ribbon ribbon-green">New</div>
                    </div>
                    <h2 class="pricing-plan-title">Mega</h2>
                    <p class="pricing-plan-price">$199<span>/mo</span></p>
                    <ul class="pricing-plan-features">
                        <li><strong>Unlimited</strong> users</li>
                        <li><strong>Unlimited</strong> projects</li>
                        <li><strong>100GB</strong> storage</li>
                    </ul>
                    <a href="index.html" class="btn">Order Now</a>
                </div>
            </div>
            <!-- End Pricing Plans Wrapper -->
        </div>
    </div>
</div>
<!-- End Pricing Table -->

<!-- Our Clients -->
<div class="section">
    <div class="container">
        <h2>Our Clients</h2>
        <div class="clients-logo-wrapper text-center row">
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/canon.png" alt="Client Name"></a></div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/cisco.png" alt="Client Name"></a></div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/dell.png" alt="Client Name"></a></div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/ea.png" alt="Client Name"></a></div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/ebay.png" alt="Client Name"></a></div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/facebook.png" alt="Client Name"></a></div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/google.png" alt="Client Name"></a></div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/hp.png" alt="Client Name"></a></div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/microsoft.png" alt="Client Name"></a></div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/mysql.png" alt="Client Name"></a></div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/sony.png" alt="Client Name"></a></div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/yahoo.png" alt="Client Name"></a></div>
        </div>
    </div>
</div>
<!-- End Our Clients -->
