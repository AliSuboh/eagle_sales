
<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Products';
//$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['/Products']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>What We Provide </h1>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <?php
        if(isset($categories))
        foreach ($categories     as $key => $category) {?>
        <div class="row service-wrapper-row">
            <div class="col-sm-4">
                <div class="service-image">
                    <img src="<?=$category['PICTURE']?>" alt="Color Schemes">
                </div>
            </div>
            <div class="col-sm-8">
                <h3><?=$category['CATEGORY_NAME_EN']?> </h3>
                <p>
                    <?=$category['DESCRIPTION_EN']?>
                </p>
                <a href="/ProductsList/<?=$category['CATEGORY_ID']?>" class="btn">Choose Product</a>
            </div>
        </div>
        <?php } ?>

    </div>
</div>
