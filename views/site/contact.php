<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Contact Us';
$this->params['breadcrumbs'][] = ['label' => 'Contact Us', 'url' => ['/Contact-Us']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Contact Us</h1>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                <!-- Map -->
                <div id="contact-us-map">

                </div>
                <!-- End Map -->
                <!-- Contact Info -->
                <p class="contact-us-details">
                    <b>Address:</b> Jordan- Amman <br/>
                    <b>Phone:</b>   +962 787171848<br/>
                    <b>Fax:</b>  +962 787171848<br/>
                    <b>Email:</b> <a href="Salah.as72@gmail.com">Salah.as72@gmail.com</a>
                </p>
                <!-- End Contact Info -->
            </div>
            <div class="col-sm-5">
                <!-- Contact Form -->
                <h3>Send Us a Message</h3>
                <div class="contact-form-wrapper">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="Name" class="col-sm-3 control-label"><b>Your name</b></label>
                            <div class="col-sm-9">
                                <input class="form-control" id="Name" type="text" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contact-email" class="col-sm-3 control-label"><b>Your Email</b></label>
                            <div class="col-sm-9">
                                <input class="form-control" id="contact-email" type="text" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contact-message" class="col-sm-3 control-label"><b>Select Topic</b></label>
                            <div class="col-sm-9">
                                <select class="form-control" id="prependedInput">
                                    <option>Please select topic...</option>
                                    <option>General</option>
                                    <option>Services</option>
                                    <option>Orders</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contact-message" class="col-sm-3 control-label"><b>Message</b></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="5" id="contact-message"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn pull-right">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- End Contact Info -->
            </div>
        </div>
    </div>
</div>