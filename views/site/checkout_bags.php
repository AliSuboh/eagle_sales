
<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Checkout';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Checkout</h1>
            </div>
        </div>
    </div>
</div>
<div class="row_new">
    <div class="col-75_new">
        <div class="container_new">
            <form  onsubmit="return checkForm(this);"  name="paymentForm" accept-charset="utf-8" method="post" autocomplete='off'>
                <input type="hidden" name="_csrf" value="--><?=Yii::$app->request->getCsrfToken()?>" />
                <div class="row">
                    <div class="col-50_new">
                        <h3>Billing Address</h3>
                        <label for="fname"><i class="fa fa-user"></i> Full Name</label>
                        <input type="text" id="fname" name="firstname" placeholder="John M. Doe">
                        <label for="email"><i class="fa fa-envelope"></i> Email</label>
                        <input type="text" id="email" name="email" placeholder="john@example.com">
                        <label for="adr"><i class="fa fa-address-card-o"></i> Address</label>
                        <input type="text" id="adr" name="address" placeholder="542 W. 15th Street">
                        <label for="city"><i class="fa fa-institution"></i> City</label>
                        <input type="text" id="city" name="city" placeholder="New York">

                        <div class="row">

                        </div>
                    </div>

                    <div class="col-50_new">
                        <h3>Payment</h3>
                        <label for="fname">Accepted Cards</label>
                        <div class="icon-container">
                            <i class="fa fa-cc-visa" style="color:navy;"></i>
<!--                            <i class="fa fa-cc-amex" style="color:blue;"></i>-->
                            <i class="fa fa-cc-mastercard" style="color:red;"></i>
<!--                            <i class="fa fa-cc-discover" style="color:orange;"></i>-->
                        </div>
                        <label for="cname">Name on Card</label>
                        <input type="text" id="cname" name="cardname" placeholder="John More Doe">
                        <label for="ccnum">Credit card number</label>
                        <input type="text" id="ccnum" name="cardnumber" placeholder="1111-2222-3333-4444">
                        <label for="expmonth">Exp Month</label>
                        <input type="text" id="expmonth" name="expmonth" placeholder="September">
                        <div class="row">
                            <div class="col-50_new">
                                <label for="expyear">Exp Year</label>
                                <input type="text" id="expyear" name="expyear" placeholder="2018">
                            </div>
                            <div class="col-50_new">
                                <label for="cvv">CVV</label>
                                <input type="text" id="cvv" name="cvv" placeholder="352">
                            </div>
                        </div>
                    </div>

                </div>
                <p><input type="checkbox" required name="terms"> I accept the <u>Terms and Conditions</u></p>

                <input type="submit" value="Continue to checkout" class="btn_new">
            </form>
        </div>
    </div>
    <div class="col-25_new">
        <div class="container_new">
            <h4>Cart <span class="price" style="color:black"><i class="fa fa-shopping-cart"></i> <b><?=count($cart_bag)?></b></span></h4>
            <?php if(isset($cart_bag) && !empty($cart_bag)){
                foreach ($cart_bag as $key => $product){
                    ?>
                    <p><a href="/Details/<?=$product['PRODUCT_ID']?>"><?=$product['PRODUCT_NAME_EN']?></a> <span class="price">$<?=$product['PRODUCT_PRICE']?></span></p>

                <?php }}  ?>

            <hr>
            <p>Sub Total <span class="price" style="color:black"><b>$<?=$sub_total?></b></span></p>
            <p>Discount <span class="price" style="color:black"><b>- $<?=$discount?></b></span></p>
            <hr>
            <p>Total <span class="price" style="color:black"><b>$<?=$total_amount?></b></span></p>
        </div>
    </div>
</div>
