
<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'shopping-cart';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Shopping Cart</h1>
            </div>
        </div>
    </div>
</div>


<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Action Buttons -->
                <div class="pull-right">
                    <a class="btn btn-grey refresh"><i class="glyphicon glyphicon-refresh "></i> UPDATE</a>
                        <a href="/payment"  class="btn checkoutBtn" <?=empty($cart_bag)?'disabled':''?>><i class="glyphicon glyphicon-shopping-cart icon-white"></i> CHECK OUT</a>
                </div>
            </div>
        </div>
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
        <input type="hidden" id="cart_bag" name="cart_bag" value="<?=(!empty($cart_bag) && count($cart_bag) > 0)?1:0?>" />

        <div class="row">
            <div class="col-md-12">
                <!-- Shopping Cart Items -->
                <table class="shopping-cart">

                    <?php if(isset($cart_bag) && !empty($cart_bag)){
                        foreach ($cart_bag as $key => $product){
                        ?>
                    <!-- Shopping Cart Item -->
                    <tr>
                        <!-- Shopping Cart Item Image -->
                        <td class="image"><a href="page-product-details.html"><img src="<?=$product['PICTURE']?>" alt="Item Name"></a></td>
                        <!-- Shopping Cart Item Description & Features -->
                        <td>
                            <div class="cart-item-title"><a href="/Details/<?=$product['PRODUCT_ID']?>"><?=$product['PRODUCT_NAME_EN']?></a></div>

                        </td>
                        <!-- Shopping Cart Item Quantity -->
                        <td class="quantity">
                            <input class="form-control input-sm input-micro" type="number" id="quantities" data-key="<?=$key?>" value="1" min="1" max="5">
                        </td>
                        <!-- Shopping Cart Item Price -->
                        <td class="price"  >
                            <label id="price<?=$key?>" class="price" > $<?=$product['PRODUCT_PRICE']?></label>
                            <input type="hidden" id="p_price<?=$key?>" name="p_price" class="p_price" value="<?=$product['PRODUCT_PRICE']?>" />
                        </td>

                        <input type="hidden" id="discount<?=$key?>" class="discount" value="<?=$discounted_array[$key]['discount']?>" />
                        <!-- Shopping Cart Item Actions -->
                        <td class="actions">
                            <button href="#" class="btn btn-xs btn-grey"><i class="glyphicon glyphicon-pencil"></i></button>
                            <button  class="btn btn-xs btn-grey removefromcart remove_<?=$product['PRODUCT_ID']?>" value="<?=$product['PRODUCT_ID']?>"><i class="glyphicon glyphicon-trash"></i></button>
                        </td>
                    </tr>
                    <!-- End Shopping Cart Item -->

                    <?php }}  ?>


                </table>
                <!-- End Shopping Cart Items -->
            </div>
        </div>
        <div class="row">

            <div class="col-md-4  col-sm-6 col-sm-offset-6 new-col-md">
                <table class="cart-totals">
                    <tr>
                        <td><b>Discount</b></td>
                        <td><label id="total_discount">- $<?=$total_discount?></label></td>
                    </tr>
                    <tr class="cart-grand-total">
                        <td><b>Total</b></td>

                        <td><label id="total_price">$<?=$total_amount?></label></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</div>