<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = ['label' => 'About Us', 'url' => ['/About']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <!-- Page Title -->
    <div class="section section-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>About Us</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h3>We are leading company</h3>
                    <p>
                        Since then it has gotten busy in making a variety of software. Through it, the directors gained varied experiences and became more convinced on the urgent need for spreading within the surrounding markets perimeters, Therefore, the team of Eagle started hovering with its products and services, to appear in the Arabian Gulf sky of businesses, and once in awhile, analysing and determining specified market needs.
                    </p>
                    <h3>Wide range of services</h3>
                    <p>
                        Supported by the latest programming languages, Eagle produced a
                        fully integrated ERP system that serves all kinds of administration, financial
                        and inventory issues effectively and up to the highest expectations, and that
                        is due to:
                    </p>
                    <ul>
                        <li>
                            EAGLE has been designed and developed using ORACLE 11g Database
                        </li>
                        <li>
                            EAGLE is a web based system developed using Oracle 10g forms and
                            Reports.
                        </li>
                        <li>
                            System supports multiple concurrent users access
                        </li>
                        <li>
                            EAGLE ERP system supports both Arabic and English languages
                        </li>
                        <li>
                            Provides Graphical User Interface (GUI) which is simple and easy to use
                        </li>
                        <li>
                            System maintains data (security and privacy) through multilevel
                            authorization procedures.
                        </li>
                        <li>
                            EAGLE preserves its’ reliability and performance even with the increasing
                            number of records
                        </li>
                        <li>
                            All system reports are generated in PDF formats which facilitate their
                            portability and archiving away of the system, without the need of
                            conversion.
                        </li>
                        <li>
                            System simplifies user interaction process through a dynamic favorite
                            shortcuts list, which enables the user to access his/her favorite screens
                            easily and quickly.
                        </li>

                    </ul>
                </div>
                <div class="col-sm-6">
                    <div class="video-wrapper">
                        <iframe src="http://player.vimeo.com/video/47000322?title=0&amp;byline=0&amp;portrait=0" width="500" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Press Coverage
    <div class="section">
        <div class="container">
            <h2>Featured on</h2>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="in-press press-wired">
                        <a href="#">Morbi eleifend congue elit nec sagittis. Praesent aliquam lobortis tellus, nec consequat vitae</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="in-press press-mashable">
                        <a href="#">Morbi eleifend congue elit nec sagittis. Praesent aliquam lobortis tellus, nec consequat vitae</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="in-press press-techcrunch">
                        <a href="#">Morbi eleifend congue elit nec sagittis. Praesent aliquam lobortis tellus, nec consequat vitae</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
   Press Coverage -->

    <div class="section">
        <div class="container">
            <h2>Our Services</h2>
            <div class="row">
                <?php
                if(isset($categories))
                foreach ($categories     as $key => $category) {?>

                    <div class="col-md-5  col-sm-5">
                        <div class="service-wrapper">
                            <img src="<?=$category['PICTURE']?>" style="width: 100%" alt="Service Name">
                            <h3><?=$category['CATEGORY_NAME_EN']?></h3>
                            <ul class="services_li" >
                                <li ><?=$category['DESCRIPTION_EN']?><a href="/ProductsList/<?=$category['CATEGORY_ID']?>">Read more...</a></li>
                            </ul>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>
