<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
/* @var $form ActiveForm */
$this->title = 'Payment Info';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment">
    <div class="section section-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Payment Info</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="row_new">
        <div class="col-75_new">
            <div class="container_new">
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-50_new">
                        <h3>Billing Address</h3>
                        <?= $form->field($model, 'cardname')->textInput(['placeholder' => "John M. Doe"])->label('<i class="fa fa-user"></i> Name on Card') ?>
                        <?= $form->field($model, 'email')->textInput(['placeholder' => "john@example.com"])->label('<i class="fa fa-envelope"></i> Email') ?>
                        <?= $form->field($model, 'adr')->textInput(['placeholder' => "542 W. 15th Street"])->label('<i class="fa fa-address-card-o"></i> Address') ?>
                        <?= $form->field($model, 'city')->textInput(['placeholder' => "New York"])->label('<i class="fa fa-institution"></i> City') ?>
                        <div class="row">

                        </div>
                    </div>
                    <div class="col-50_new">
                        <h3>Payment</h3>
                        <label for="fname">Accepted Cards</label>
                        <div class="icon-container">
                            <i class="fa fa-cc-visa" style="color:navy;"></i>
                            <i class="fa fa-cc-mastercard" style="color:red;"></i>
                        </div>
                        <?= $form->field($model, 'cardnumber')->textInput(['placeholder' => "1111-2222-3333-4444"])->label('Credit card number') ?>
                        <?= $form->field($model, 'cvv')->textInput(['placeholder' => "352"])->label('CVV') ?>

                        <div class="row">
                            <div class="col-50_new">
                                <?= $form->field($model, 'expyear')->textInput(['placeholder' => "2018"])->label('Exp Year') ?>
                            </div>
                            <div class="col-50_new">
                                <?= $form->field($model, 'expmonth')->textInput(['placeholder' => "1111-2222-3333-4444"])->label('Exp Month') ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="col-25_new">
            <div class="container_new">
                <h4>Cart <span class="price" style="color:black"><i
                                class="fa fa-shopping-cart"></i> <b><?= count($cart_bag) ?></b></span></h4>
                <?php if (isset($cart_bag) && !empty($cart_bag)) {
                    foreach ($cart_bag as $key => $product) {
                        ?>
                        <p><a href="/Details/<?= $product['PRODUCT_ID'] ?>"><?= $product['PRODUCT_NAME_EN'] ?></a> <span
                                    class="price">$<?= $product['PRODUCT_PRICE'] ?></span></p>
                    <?php }
                } ?>
                <hr>
                <p>Sub Total <span class="price" style="color:black"><b>$<?= $sub_total ?></b></span></p>
                <p>Discount <span class="price" style="color:black"><b>- $<?= $discount ?></b></span></p>
                <hr>
                <p>Total <span class="price" style="color:black"><b>$<?= $total_amount ?></b></span></p>
            </div>
        </div>
    </div>


</div><!-- payment -->
